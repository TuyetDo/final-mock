import { Article } from "./../../model/articleType";
import { ArticleActionType } from "../../model/actionType";
import { ArticleAction } from "../action/actionObjects/ArticleAction";
import { PaginationAction } from "../action/actionObjects/PaginationAction";
import { ArticleTrendingAction } from "../action/actionObjects/ArticleTrendingAction";

const initialState: Article[] = [];

const articleReducer = (
  state = initialState,
  action: ArticleAction | PaginationAction | ArticleTrendingAction
) => {
  switch (action.type) {
    case ArticleActionType.GET_ARTICLE:
      return state;
    case ArticleActionType.GET_ARTICLE_SUCCESS:
      return action.payload;
    case ArticleActionType.GET_ARTICLE_ERROR:
      return action.payload;
    case ArticleActionType.GET_ARTICLE_PAGINATION:
      return state;
    case ArticleActionType.GET_ARTICLE_PAGINATION_SUCCESS:
      return action.payload;
    case ArticleActionType.GET_ARTICLE_PAGINATION_ERROR:
      return action.payload;
    case ArticleActionType.GET_ARTICLE_TRENDING:
      return state;
    case ArticleActionType.GET_ARTICLE_TRENDING_SUCCESS:
      return action.payload;
    case ArticleActionType.GET_ARTICLE_TRENDING_ERROR:
      return action.payload;

    default:
      return state;
  }
};

export default articleReducer;
