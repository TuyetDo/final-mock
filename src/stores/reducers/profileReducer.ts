import { ProfileActionType } from '../../model/actionType';
import { ProfileAction } from '../action/actionObjects/ProfileAction';




const initialState: string[] = []

const profileReducer = (state = initialState, action: ProfileAction) => {
    switch (action.type) {
        case ProfileActionType.GET_PROFILE:
            return [...state]
        case ProfileActionType.GET_PROFILE_SUCCESS:
            return [...state, action.payload]
        case ProfileActionType.GET_PROFILE_ERROR:
            return [...state, action.payload]
        default:
            return state
    }
}

export default profileReducer