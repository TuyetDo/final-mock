import { Article } from './../../model/articleType';
import { ArticleActionType } from "../../model/actionType"
import { ArticleFeedAction } from '../action/actionObjects/FeedAction';


const initialState: Article[] = [];


const articleTrendingReducer = (state = initialState, action: ArticleFeedAction) => {
    switch (action.type) {
        case ArticleActionType.GET_ARTICLE_FEED:
            return state
        case ArticleActionType.GET_ARTICLE_FEED_SUCCESS:
            return action.payload
        case ArticleActionType.GET_ARTICLE_FEED_ERROR:
            return action.payload
        default:
            return state
    }
}

export default articleTrendingReducer
