import { ArticleActionType } from "../../model/actionType";
import { Article } from "../../model/articleType";
import { ArticleAction } from "../action/actionObjects/ArticleAction";

const initialState: Article = {
  title: "",
  slug: "",
  body: "",
  createdAt: "",
  updatedAt: "",
  tagList: [],
  description: "",
  author: {
    username: "",
    bio: "",
    image: "",
    following: false,
  },
  favorited: false,
  favoritesCount: 0,
};

export const articleSlugReducer = (
  state = initialState,
  action: ArticleAction
) => {
  switch (action.type) {
    case ArticleActionType.GET_ARTICLE_SLUG:
      return {
        ...state,
        // article: action.payload,
      };
    case ArticleActionType.GET_ARTICLE_SLUG_SUCCESS:
      return action.payload;

    case ArticleActionType.GET_ARTICLE_SLUG_ERROR:
      return action.payload;

    default:
      return {
        ...state,
      };
  }
};
