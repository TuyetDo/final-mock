import { CommentActionType } from "../../model/actionType";
import { Comments } from "../../model/articleType";
import { CommentAction } from "../action/actionObjects/CommentAction";

const initialStateGetComments: Comments[] = [];
const initialStateCreateComment: Comments = {
  author: {
    username: "",
    bio: "",
    image: "",
    following: false,
  },
  id: 0,
  updatedAt: "",
  createdAt: "",
  body: "",
};

const initialStateDeleteComment = {};

export const commentGetReducer = (
  state = initialStateGetComments,
  action: CommentAction
) => {
  switch (action.type) {
    case CommentActionType.GET_COMMENT:
      return {
        ...state,
      };
    case CommentActionType.GET_COMMENT_SUCCESS:
      return {
        ...state,
        comments: action.payload,
      };
    case CommentActionType.GET_COMMENT_ERROR:
      return {
        ...state,
        error: action.payload,
      };
    default:
      return state;
  }
};

export const commentCreateReducer = (
  state = initialStateCreateComment,
  action: CommentAction
) => {
  switch (action.type) {
    case CommentActionType.CREATE_COMMENT:
      return {
        ...state,
        commentResponse: null,
      };
    case CommentActionType.CREATE_COMMENT_SUCCESS:
      const data = action.payload;
      return {
        ...state,
        commentResponse: data,
      };
    case CommentActionType.CREATE_COMMENT_ERROR:
      return {
        ...state,
        commentResponse: action.payload,
      };

    default:
      return {
        ...state,
      };
  }
};

export const commentDeleteReducer = (
  state = initialStateDeleteComment,
  action: CommentAction
) => {
  switch (action.type) {
    case CommentActionType.DELETE_COMMENT:
      return {
        ...state,
        delete_id: action.payload,
      };
    case CommentActionType.DELETE_COMMENT_SUCCESS:
      return {
        ...state,
        delete_id: action.payload,
      };
    case CommentActionType.DELETE_COMMENT_ERROR:
      return {
        ...state,
        error: action.payload,
      };

    default:
      return {
        ...state,
      };
  }
};
