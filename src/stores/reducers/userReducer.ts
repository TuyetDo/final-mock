import { UserActionType } from "../../model/actionType";
import { StateReducer } from "../../model/stateReducerType";
import { UserAction } from "../action/actionObjects/AuthAction";

const initialState: StateReducer = {
  error: null,
  email: null,
  username: null,
  image: null,
  isLogin: false,
  bio: null,
};

export const userReducer = (
  state = initialState,
  action: UserAction
): StateReducer => {
  switch (action.type) {
    case UserActionType.LOGIN_SUCCESS:
      return {
        ...state,
        email: action.payload.email,
        username: action.payload.username,
        image: action.payload.image,
        isLogin: true,
        success: { success: true },
      };
    case UserActionType.LOGIN_ERROR:
      return {
        ...state,
        error: action.payload.error,
        isLogin: false,
      };
    case UserActionType.REGISTER_SUCCESS:
      return {
        ...state,
        email: action.payload.email,
        username: action.payload.username,
        image: action.payload.image,
        isLogin: true,
        success: { success: true },
      };
    case UserActionType.REGISTER_ERROR:
      return {
        ...state,
        error: action.payload.error,
        isLogin: false,
      };
    case UserActionType.GET_USER_SUCCESS:
      return {
        error: null,
        email: action.payload.email,
        username: action.payload.username,
        image: action.payload.image,
        isLogin: true,
        bio: action.payload.bio,
      };
    case UserActionType.LOGOUT:
      localStorage.removeItem("token");
      return {
        ...initialState,
        isLogin: false,
      };
    case UserActionType.UPDATE_USER_SUCCESS:
      return {
        ...state,
        error: null,
        email: action.payload.email,
        username: action.payload.username,
        image: action.payload.image,
        bio: action.payload.bio,
        success: { success: true },
      };
    case UserActionType.UPDATE_USER_ERROR:
      return {
        ...state,
        error: action.payload,
      };
    default:
      return state;
  }
};
