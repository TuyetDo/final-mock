import { TagsActionType } from '../../model/actionType';
import { TagsAction } from '../action/actionObjects/TagsAction';




const initialState: string[] = []

const tagsReducer = (state = initialState, action: TagsAction) => {
    switch (action.type) {
        case TagsActionType.GET_TAGS:
            return state
        case TagsActionType.GET_TAGS_SUCCESS:
            return action.payload
        case TagsActionType.GET_TAGS_ERROR:
            return action.payload
        default:
            return state
    }
}

export default tagsReducer