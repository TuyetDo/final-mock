import { combineReducers } from "redux";
import articleReducer from "./articleReducer";
import { articleSlugReducer } from "./articleSlugReducer";
import articleTrendingReducer from "./articleTrendingReducer";
import {
  commentCreateReducer,
  commentDeleteReducer,
  commentGetReducer,
} from "./commentReducer";
import feedReducer from "./feedReducer";

import { favoriteArticleReducer, followArticleReducer } from "./followAndFavoriteReducer";

import loadingReducer from "./loadingReducer";
import { notifyReducer } from "./notifyReducer";
import profileReducer from "./profileReducer";
import tagsReducer from "./tagsReducer";
import { userReducer } from "./userReducer";

const rootReducer: any = combineReducers({
  user: userReducer,
  isLoading: loadingReducer,
  articles: articleReducer,
  trending: articleTrendingReducer,
  tags: tagsReducer,
  profile: profileReducer,
  article: articleSlugReducer,
  comments: commentGetReducer,
  comment: commentCreateReducer,
  delete: commentDeleteReducer,
  feeds: feedReducer,
  favorited: favoriteArticleReducer,
  followed: followArticleReducer,
  notification: notifyReducer,
});

export default rootReducer;

export type RootState = ReturnType<typeof rootReducer>;
