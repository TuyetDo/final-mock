import { Article } from './../../model/articleType';
import { ArticleActionType } from "../../model/actionType"
import { ArticleTrendingAction } from '../action/actionObjects/ArticleTrendingAction';




const initialState: Article[] = []

const articleTrendingReducer = (state = initialState, action: ArticleTrendingAction) => {
    switch (action.type) {

        case ArticleActionType.GET_ARTICLE_TRENDING:
            return state
        case ArticleActionType.GET_ARTICLE_TRENDING_SUCCESS:
            return action.payload
        case ArticleActionType.GET_ARTICLE_TRENDING_ERROR:
            return action.payload
        default:
            return state
    }
}

export default articleTrendingReducer