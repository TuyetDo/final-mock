import { StarHalfTwoTone, StarRateOutlined } from "@material-ui/icons";
import { ArticleActionType } from "../../model/actionType";
import { Article, Author } from "../../model/articleType";
import { ArticleAction } from "../action/actionObjects/ArticleAction";

const initialStateFollow: Author = {
  username: "",
  image: "",
  bio: "",
  following: false,
}

const initialStateFavorite: Article = {
  title: "",
  slug: "",
  body:"",
  createdAt: "",
  updatedAt: "",
  tagList: [],
  description: "",
  author: initialStateFollow,
  favorited: false,
  favoritesCount: 0,
}

export const favoriteArticleReducer = (state = initialStateFavorite, action: ArticleAction) => {
  switch(action.type){
    case ArticleActionType.CREATE_FAVORITE_ARTICLE:
      return state
    case ArticleActionType.CREATE_FAVORITE_ARTICLE_SUCCESS:
      return action.payload
    case ArticleActionType.CREATE_FAVORITE_ARTICLE_ERROR:
      return action.payload
    default:
      return state
  }
}

export const followArticleReducer = (state = initialStateFollow, action: ArticleAction) => {
  switch(action.type){
    case ArticleActionType.CREATE_FOLLOW_ARTICLE:
      return state
    case ArticleActionType.CREATE_FOLLOW_ARTICLE_SUCCESS:
      return action.payload
    case ArticleActionType.CREATE_FOLLOW_ARTICLE_ERROR:
      return action.payload
    default:
      return state
  }
}
