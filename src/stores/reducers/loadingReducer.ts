import { LoadingActionType } from "../../model/actionType";
import { LoadAction } from "../action/actionObjects/LoadingAction";





const loadingReducer = (state: boolean = true, action: LoadAction) => {
    switch (action.type) {
        case LoadingActionType.LOADING:
            return action.payload
        case LoadingActionType.LOAD_SUCCESS:
            return action.payload
        case LoadingActionType.LOAD_ERROR:
            return action.payload
        default:
            return state;
    }
};

export default loadingReducer;