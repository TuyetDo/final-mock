import { ArticleActionType } from "../../model/actionType";

const initialState = {
  notify: false,
};

export const notifyReducer = (state = initialState, action: any): any => {
  switch (action.type) {
    case ArticleActionType.DELETE_ARTICLE:
      return {
        notify: true,
      };
    case ArticleActionType.CANCEL_DELETE_ARTICLE:
      return {
        notify: false,
      };
    case ArticleActionType.DELETE_ARTICLE_CONFIRM:
      return {
        notify: false,
      };
    default:
      return state;
  }
};
