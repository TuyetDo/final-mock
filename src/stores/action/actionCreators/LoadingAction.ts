import { LoadingActionType } from "../../../model/actionType"



export const loadingData = () => {
    return {
        type: LoadingActionType.LOADING,
        payload: true
    }
}
export const loadingDataSuccess = () => {
    return {
        type: LoadingActionType.LOAD_SUCCESS,
        payload: false
    }
}
export const loadingDataError = () => {
    return {
        type: LoadingActionType.LOAD_ERROR,
        payload: false
    }
}