import { ArticlesResponse } from '../../../model/articleType';
import { ArticleActionType } from './../../../model/actionType';

export const getPageArticles = (offset: number, author?: string) => {
    return {
        type: ArticleActionType.GET_ARTICLE_PAGINATION,
        payload: {offset, author}
    };
};

export const getPageArticlesSuccess = (articles: ArticlesResponse) => {
    return {
        type: ArticleActionType.GET_ARTICLE_PAGINATION_SUCCESS,
        payload: articles
    };
};

export const getPageArticlesError = (error: ArticlesResponse) => {
    return {
        type: ArticleActionType.GET_ARTICLE_PAGINATION_ERROR,
        payload: error
    };
};

