import { UserActionType } from "../../../model/actionType";
import {
  userInputInfo,
  userAuthResponse,
  userAuthUpdate,
} from "../../../model/userType";

export const login = (user: userInputInfo) => {
  return {
    type: UserActionType.LOGIN,
    payload: {
      user: {
        email: user.email,
        password: user.password,
      },
    },
  };
};

export const loginSuccess = (user: userAuthResponse) => {
  return {
    type: UserActionType.LOGIN_SUCCESS,
    payload: {
      username: user.username,
      email: user.email,
      token: user.token,
    },
  };
};

export const loginError = (dataError: any) => {
  return {
    type: UserActionType.LOGIN_ERROR,
    payload: {
      error: dataError.error["email or password"],
    },
  };
};

export const register = (user: userInputInfo) => {
  return {
    type: UserActionType.REGISTER,
    payload: {
      user: {
        username: user.username,
        email: user.email,
        password: user.password,
      },
    },
  };
};

export const registerSuccess = (user: userAuthResponse) => {
  return {
    type: UserActionType.REGISTER_SUCCESS,
    payload: {
      username: user.username,
      email: user.email,
      token: user.token,
    },
  };
};

export const registerError = (dataError: any) => {
  return {
    type: UserActionType.REGISTER_ERROR,
    payload: {
      error: dataError.error,
    },
  };
};

export const getUser = () => {
  return {
    type: UserActionType.GET_USER,
    payload: {},
  };
};

export const getUserSuccess = (user: userAuthResponse) => {
  return {
    type: UserActionType.GET_USER_SUCCESS,
    payload: {
      email: user.email,
      username: user.username,
      bio: user.bio,
      image: user.image,
    },
  };
};

export const getUserError = () => {
  return {
    type: UserActionType.GET_USER_ERROR,
    payload: {},
  };
};

export const logout = () => {
  return {
    type: UserActionType.LOGOUT,
    payload: {},
  };
};

export const updateUser = (user: userAuthUpdate) => {
  return {
    type: UserActionType.UPDATE_USER,
    payload: {
      user: {
        username: user.username,
        email: user.email,
        password: user.password,
        image: user.image,
        bio: user.bio,
      },
    },
  };
};

export const updateUserSuccess = (user: userAuthUpdate) => {
  return {
    type: UserActionType.UPDATE_USER_SUCCESS,
    payload: {
      username: user.username,
      email: user.email,
      password: user.password,
      image: user.image,
      bio: user.bio,
    },
  };
};

export const updateUserError = (errors: any) => {
  return {
    type: UserActionType.UPDATE_USER_ERROR,
    payload: errors,
  };
};
