import { ArticlesResponse } from '../../../model/articleType';
import { ArticleActionType } from './../../../model/actionType';



// get trending article

export const getArticleTrending = (tag: string) => {
    return {
        type: ArticleActionType.GET_ARTICLE_TRENDING,
        payload: tag
    };
};

export const getArticleTrendingSuccess = (articles: ArticlesResponse) => {
    return {
        type: ArticleActionType.GET_ARTICLE_TRENDING_SUCCESS,
        payload: articles

    };
};

export const getArticleTrendingError = (error: ArticlesResponse) => {
    return {
        type: ArticleActionType.GET_ARTICLE_TRENDING_ERROR,
        payload: error
    };
};
