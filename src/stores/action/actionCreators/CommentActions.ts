import { CommentActionType } from "../../../model/actionType"
import { CommentCreate, Comments, CommentsResponse } from "../../../model/articleType"

// get comment
export const getComments = (commentId: string) => {
  return {
    type: CommentActionType.GET_COMMENT,
    payload: commentId
  }
}

export const getCommentsSuccess = (comments: CommentsResponse) => {
  return {
    type: CommentActionType.GET_COMMENT_SUCCESS,
    payload: comments
  }
}

export const getCommentsError = (error: CommentsResponse) => {
  return {
    type: CommentActionType.GET_COMMENT_ERROR,
    payload: error
  }
}

// create comment
export const createComment = (comment: CommentCreate, commentId: string) =>{
  return {
    type: CommentActionType.CREATE_COMMENT,
    payload: {comment, commentId}
  }

}

export const createCommentSuccess = (comments: Comments) => {
  return {
    type: CommentActionType.CREATE_COMMENT_SUCCESS,
    payload: comments
  }
}

export const createCommentError = (error: Comments) => {
  return {
    type: CommentActionType.CREATE_COMMENT_ERROR,
    payload: error
  }
}

export const deleteComment = (slug: string, commentId: number) => {
  return {
    type: CommentActionType.DELETE_COMMENT,
    payload: {
      slug,
      commentId
    }
  }
}

export const deleteCommentSuccess = (data: number) => {
  return {
    type: CommentActionType.DELETE_COMMENT_SUCCESS,
    payload: data
  }
}

export const deleteCommentError = () => {
  return {
    type: CommentActionType.DELETE_COMMENT_ERROR,
    payload: null
  }
}
