import { ProfileActionType } from "../../../model/actionType";
import { ProfileResponse } from "../../../model/profileType";

export const getProfile = (user: string) => {
    return {
        type: ProfileActionType.GET_PROFILE,
        payload: user
    };
};

export const getProfileSuccess = (profile: ProfileResponse) => {
    return {
        type: ProfileActionType.GET_PROFILE_SUCCESS,
        payload: profile

    };
};

export const getProfileError = (error: ProfileResponse) => {
    return {
        type: ProfileActionType.GET_PROFILE_ERROR,
        payload: error
    };
};

