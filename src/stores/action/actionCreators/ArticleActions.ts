import {
  Article,
  ArticleFavorite,
  ArticleSlug,
  ArticlesResponse,
} from "../../../model/articleType";
import { ProfileResponse } from "../../../model/profileType";
import { ArticleActionType } from "./../../../model/actionType";

export const getArticles = () => {
  return {
    type: ArticleActionType.GET_ARTICLE,
  };
};

export const getArticlesSuccess = (articles: ArticlesResponse) => {
  return {
    type: ArticleActionType.GET_ARTICLE_SUCCESS,
    payload: articles,
  };
};

export const getArticlesError = (error: ArticlesResponse) => {
  return {
    type: ArticleActionType.GET_ARTICLE_ERROR,
    payload: error,
  };
};

// Article by slug
export const getArticleSlug = (data: string) => {
  return {
    type: ArticleActionType.GET_ARTICLE_SLUG,
    payload: data,
  };
};

export const getArticleSlugSuccess = (article: Article) => {
  return {
    type: ArticleActionType.GET_ARTICLE_SLUG_SUCCESS,
    payload: article,
  };
};

export const getArticleSlugError = (error: ArticleSlug | ProfileResponse) => {
  return {
    type: ArticleActionType.GET_ARTICLE_SLUG_ERROR,
    payload: error,
  };
};

// favorited
export const favoriteArticle = (slug: string, checked: boolean) => {
  return {
    type: ArticleActionType.CREATE_FAVORITE_ARTICLE,
    payload: { slug, checked },
  };
};

export const favoriteArticleSuccess = (article: ArticleFavorite) => {
  return {
    type: ArticleActionType.CREATE_FAVORITE_ARTICLE_SUCCESS,
    payload: article,
  };
};

export const favoriteArticleError = (article: ArticlesResponse) => {
  return {
    type: ArticleActionType.CREATE_FAVORITE_ARTICLE_ERROR,
    payload: article,
  };
};

// Follow article
export const followArticle = (username: string, followed: boolean, slug?: string) => {
  return {
    type: ArticleActionType.CREATE_FOLLOW_ARTICLE,
    payload: { username, followed, slug },
  };
};

export const followArticleSuccess = (profile: ProfileResponse) => {
  return {
    type: ArticleActionType.CREATE_FOLLOW_ARTICLE_SUCCESS,
    payload: profile,
  };
};

export const followArticleError = (profile: ProfileResponse) => {
  return {
    type: ArticleActionType.CREATE_FOLLOW_ARTICLE_ERROR,
    payload: profile,
  };
};

export const deleteArticle = (slug: string) => {
  return {
    type: ArticleActionType.DELETE_ARTICLE,
    payload: {
      slug,
    },
  };
};

export const deleteArticleNow = () => {
  return {
    type: ArticleActionType.DELETE_ARTICLE_NOW,
    payload: "",
  };
};

export const cancelDeleteArticle = () => {
  return {
    type: ArticleActionType.CANCEL_DELETE_ARTICLE,
    payload: "",
  };
};

export const deleteArticleConfirm = () => {
  return {
    type: ArticleActionType.DELETE_ARTICLE_CONFIRM,
    payload: "",
  };
};
