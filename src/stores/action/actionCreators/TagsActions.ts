import { TagsResponse } from "../../../model/tagsType";
import { TagsActionType } from './../../../model/actionType';

export const getTags = () => {
    return {
        type: TagsActionType.GET_TAGS,
    };
};

export const getTagsSuccess = (articles: TagsResponse) => {
    return {
        type: TagsActionType.GET_TAGS_SUCCESS,
        payload: articles

    };
};

export const getTagsError = (error: TagsResponse) => {
    return {
        type: TagsActionType.GET_TAGS_ERROR,
        payload: error
    };
};

