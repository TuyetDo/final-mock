import { ArticlesResponse } from '../../../model/articleType';
import { ArticleActionType } from './../../../model/actionType';



// get trending article

export const getArticleFeed = () => {
    return {
        type: ArticleActionType.GET_ARTICLE_FEED,

    };
};

export const getArticleFeedSuccess = (articles: ArticlesResponse) => {
    return {
        type: ArticleActionType.GET_ARTICLE_FEED_SUCCESS,
        payload: articles

    };
};

export const getArticleFeedError = (error: ArticlesResponse) => {
    return {
        type: ArticleActionType.GET_ARTICLE_FEED_ERROR,
        payload: error
    };
};
