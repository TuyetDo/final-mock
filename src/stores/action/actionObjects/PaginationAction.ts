import { ArticleActionType } from "../../../model/actionType";
import { ArticlesResponse } from "../../../model/articleType";
import { PaginationType } from "../../../model/paginationType";


// Get all article action

export interface GetPaginationAction {
    type: ArticleActionType.GET_ARTICLE_PAGINATION;
    payload: PaginationType
}
export interface GetPaginationSuccessAction {
    type: ArticleActionType.GET_ARTICLE_PAGINATION_SUCCESS;
    payload: ArticlesResponse;
}
export interface GetPaginationErrorAction {
    type: ArticleActionType.GET_ARTICLE_PAGINATION_ERROR;
    payload: ArticlesResponse
}

export type PaginationAction =
    | GetPaginationAction
    | GetPaginationSuccessAction
    | GetPaginationErrorAction


