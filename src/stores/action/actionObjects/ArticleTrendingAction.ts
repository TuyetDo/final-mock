import { ArticleActionType } from "../../../model/actionType";
import { ArticlesResponse } from "../../../model/articleType";


// Get article trending

export interface GetArticleTrendingAction {
    type: ArticleActionType.GET_ARTICLE_TRENDING;
    payload: null
}
export interface GetArticleTrendingSuccessAction {
    type: ArticleActionType.GET_ARTICLE_TRENDING_SUCCESS;
    payload: ArticlesResponse;
}
export interface GetArticleTrendingErrorAction {
    type: ArticleActionType.GET_ARTICLE_TRENDING_ERROR;
    payload: ArticlesResponse
}

export type ArticleTrendingAction =

    | GetArticleTrendingAction
    | GetArticleTrendingSuccessAction
    | GetArticleTrendingErrorAction

