import { ProfileActionType } from "../../../model/actionType";
import { ProfileResponse } from "../../../model/profileType";


// Get all article action

export interface GetProfileAction {
    type: ProfileActionType.GET_PROFILE;
    payload: null
}
export interface GetProfileSuccessAction {
    type: ProfileActionType.GET_PROFILE_SUCCESS;
    payload: ProfileResponse;
}
export interface GetProfileErrorAction {
    type: ProfileActionType.GET_PROFILE_ERROR;
    payload: ProfileResponse
}

export type ProfileAction =
    | GetProfileAction
    | GetProfileSuccessAction
    | GetProfileErrorAction


