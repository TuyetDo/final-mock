import { CommentActionType } from "../../../model/actionType";
import { CommentCreate, CommentDelete, Comments, CommentsResponse } from "../../../model/articleType";

// get comment
export interface GetCommentAction {
  type: CommentActionType.GET_COMMENT;
  payload: null;
}

export interface GetCommentSuccessAction {
  type: CommentActionType.GET_COMMENT_SUCCESS;
  payload: Comments[]
}

export interface GetCommentErrorAction {
  type: CommentActionType.GET_COMMENT_ERROR;
  payload: CommentsResponse
}

// create comment
export interface CreateCommentAction {
  type: CommentActionType.CREATE_COMMENT,
  payload: CommentCreate
}

export interface CreateCommentSuccessAction {
  type: CommentActionType.CREATE_COMMENT_SUCCESS,
  payload: Comments
}

export interface CreateCommentErrorAction {
  type: CommentActionType.CREATE_COMMENT_ERROR,
  payload: Comments
}

export interface DeleteCommentAction {
  type: CommentActionType.DELETE_COMMENT,
  payload: CommentDelete
}

export interface DeleteCommentSuccessAction {
  type: CommentActionType.DELETE_COMMENT_SUCCESS,
  payload: number
}

export interface DeleteCommentErrorAction {
  type: CommentActionType.DELETE_COMMENT_ERROR,
  payload: null
}

export type CommentAction =
    | GetCommentAction
    | GetCommentSuccessAction
    | GetCommentErrorAction
    | CreateCommentAction
    | CreateCommentSuccessAction
    | CreateCommentErrorAction
    | DeleteCommentAction
    | DeleteCommentSuccessAction
    | DeleteCommentErrorAction
