import { TagsActionType } from "../../../model/actionType";
import { TagsResponse } from "../../../model/tagsType";


// Get all article action

export interface GetTagsAction {
    type: TagsActionType.GET_TAGS;
    payload: null
}
export interface GetTagsSuccessAction {
    type: TagsActionType.GET_TAGS_SUCCESS;
    payload: TagsResponse;
}
export interface GetTagsErrorAction {
    type: TagsActionType.GET_TAGS_ERROR;
    payload: TagsResponse
}

export type TagsAction =
    | GetTagsAction
    | GetTagsSuccessAction
    | GetTagsErrorAction


