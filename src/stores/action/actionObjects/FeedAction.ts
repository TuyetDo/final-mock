import { ArticleActionType } from "../../../model/actionType";
import { ArticlesResponse } from "../../../model/articleType";


// Get article trending

export interface GetArticleFeedAction {
    type: ArticleActionType.GET_ARTICLE_FEED;
}
export interface GetArticleFeedSuccessAction {
    type: ArticleActionType.GET_ARTICLE_FEED_SUCCESS;
    payload: ArticlesResponse;
}
export interface GetArticleFeedErrorAction {
    type: ArticleActionType.GET_ARTICLE_FEED_ERROR;
    payload: ArticlesResponse
}

export type ArticleFeedAction =

    | GetArticleFeedAction
    | GetArticleFeedSuccessAction
    | GetArticleFeedErrorAction

