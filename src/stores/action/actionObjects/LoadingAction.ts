import { LoadingActionType } from "../../../model/actionType";
import { LoadingType } from "../../../model/LoadingType";


// Load action

export interface LoadingAction {
    type: LoadingActionType.LOADING;
    payload: LoadingType
}
export interface LoadSuccessAction {
    type: LoadingActionType.LOAD_SUCCESS;
    payload: LoadingType;
}
export interface LoadErrorAction {
    type: LoadingActionType.LOAD_ERROR;
    payload: LoadingType
}

export type LoadAction =
    | LoadingAction
    | LoadSuccessAction
    | LoadErrorAction


