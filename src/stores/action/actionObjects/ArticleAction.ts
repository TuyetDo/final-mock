import { ArticleActionType } from "../../../model/actionType";
import { Article, ArticleFavorite, ArticleFollow, ArticlesResponse, PayloadArticleFavorite } from "../../../model/articleType";


// Get all article action

export interface GetArticleAction {
    type: ArticleActionType.GET_ARTICLE;
    payload: null
}
export interface GetArticleSuccessAction {
    type: ArticleActionType.GET_ARTICLE_SUCCESS;
    payload: ArticlesResponse;
}
export interface GetArticleErrorAction {
    type: ArticleActionType.GET_ARTICLE_ERROR;
    payload: ArticlesResponse
}

export interface GetArticleSlugAction {
  type: ArticleActionType.GET_ARTICLE_SLUG,
  payload: string
}

export interface GetArticleSlugSuccessAction {
  type: ArticleActionType.GET_ARTICLE_SLUG_SUCCESS,
  payload: Article
}

export interface GetArticleSlugErrorAction {
  type: ArticleActionType.GET_ARTICLE_SLUG_ERROR,
  payload: Article
}

export interface CreateFavoriteAction {
  type: ArticleActionType.CREATE_FAVORITE_ARTICLE,
  payload: PayloadArticleFavorite
}
export interface CreateFavoriteSuccessAction {
  type: ArticleActionType.CREATE_FAVORITE_ARTICLE_SUCCESS,
  payload: ArticleFavorite
}

export interface CreateFavoriteErrorAction {
  type: ArticleActionType.CREATE_FAVORITE_ARTICLE_ERROR,
  payload: ArticleFavorite
}

// follow article
export interface CreateFollowAction {
  type: ArticleActionType.CREATE_FOLLOW_ARTICLE,
  payload: PayloadArticleFavorite
}

export interface CreateFollowSuccessAction {
  type: ArticleActionType.CREATE_FOLLOW_ARTICLE_SUCCESS,
  payload: ArticleFollow
}

export interface CreateFollowErrorAction {
  type: ArticleActionType.CREATE_FOLLOW_ARTICLE_ERROR,
  payload: ArticleFollow
}


export type ArticleAction =
    | GetArticleAction
    | GetArticleSuccessAction
    | GetArticleErrorAction
    | GetArticleSlugAction
    | GetArticleSlugSuccessAction
    | GetArticleSlugErrorAction
    | CreateFavoriteAction
    | CreateFavoriteSuccessAction
    | CreateFavoriteErrorAction
    | CreateFollowAction
    | CreateFollowSuccessAction
    | CreateFollowErrorAction


