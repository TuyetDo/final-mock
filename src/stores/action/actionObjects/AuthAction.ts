import { UserActionType } from "../../../model/actionType";
import { userAuthResponse, userInputInfo } from "../../../model/userType";

export interface LoginAction {
  type: UserActionType.LOGIN;
  payload: userInputInfo;
}

export interface LoginSuccessAction {
  type: UserActionType.LOGIN_SUCCESS;
  payload: userAuthResponse;
}

export interface LoginErrorAction {
  type: UserActionType.LOGIN_ERROR;
  payload: any;
}

export interface RegisterAction {
  type: UserActionType.REGISTER;
  payload: userInputInfo;
}

export interface RegisterSuccessAction {
  type: UserActionType.REGISTER_SUCCESS;
  payload: userAuthResponse;
}

export interface RegisterErrorAction {
  type: UserActionType.REGISTER_ERROR;
  payload: any;
}

export interface GetUserSuccessAction {
  type: UserActionType.GET_USER_SUCCESS;
  payload: userAuthResponse;
}

export interface Logout {
  type: UserActionType.LOGOUT;
  payload: {};
}

export interface UpdateUserSuccessAction {
  type: UserActionType.UPDATE_USER_SUCCESS;
  payload: userAuthResponse;
}

export interface UpdateUserErrorAction {
  type: UserActionType.UPDATE_USER_ERROR;
  payload: userAuthResponse;
}

export type UserAction =
  | LoginAction
  | LoginSuccessAction
  | LoginErrorAction
  | RegisterAction
  | RegisterSuccessAction
  | RegisterErrorAction
  | GetUserSuccessAction
  | Logout
  | UpdateUserSuccessAction
  | UpdateUserErrorAction;
