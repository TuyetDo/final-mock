import { call, put, take } from "redux-saga/effects";
import { Login, Register } from "../../configs/api/handleCallApi";
import { UserActionType } from "../../model/actionType";
import {
  loginError,
  loginSuccess,
  registerError,
  registerSuccess,
} from "../action/actionCreators/AuthActions";

export function* registerFlow() {
  while (true) {
    const { payload } = yield take(UserActionType.REGISTER);

    const response = yield call(() => {
      return Register(payload);
    });

    if (response.status === 200) {
      yield put(registerSuccess(response.data.user));
    } else if (response.status === 422) {
      yield put(
        registerError({
          error: response.data.errors,
        })
      );
    }
  }
}

export function* loginFlow() {
  while (true) {
    const { payload } = yield take(UserActionType.LOGIN);
    const response = yield call(() => {
      return Login(payload);
    });

    if (response.status === 200) {
      yield put(loginSuccess(response.data.user));
    } else if (response.status === 422) {
      yield put(
        loginError({
          error: response.data.errors,
        })
      );
    }
  }
}
