import { call, take, put } from "redux-saga/effects";
import { UpdateUser } from "../../configs/api/handleCallApi";
import { UserActionType } from "../../model/actionType";
import {
  updateUserSuccess,
  updateUserError,
} from "../action/actionCreators/AuthActions";

export function* updateUserFlow() {
  while (true) {
    const { payload } = yield take(UserActionType.UPDATE_USER);

    if (!payload.user.password) {
      delete payload.user.password;
    }

    if (!payload.user.bio) {
      delete payload.user.bio;
    }

    const response = yield call(() => {
      return UpdateUser(payload);
    });

    if (response.status === 200) {
      yield put(updateUserSuccess(response.data.user));
    } else if (response.status === 422) {
      yield put(updateUserError(response.data.errors));
    }
  }
}
