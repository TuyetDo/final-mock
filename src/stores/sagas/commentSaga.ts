import { put, takeLatest } from "redux-saga/effects";
import {
  delteCommentApi,
  getCommentsApi,
} from "../../configs/api/handleCallApi";
import { CommentActionType } from "../../model/actionType";
import { payloadCommentDelete } from "../../model/articleType";

export function* getCommentSaga(commentId: { type: string; payload: string }) {
  try {
    const data = yield getCommentsApi(commentId);
    yield put({ type: CommentActionType.GET_COMMENT_SUCCESS, payload: data });
  } catch (error) {
    yield put({ type: CommentActionType.GET_COMMENT_ERROR, payload: error });
  }
}

export function* deleteCommentSaga(data: payloadCommentDelete) {
  try {
     yield delteCommentApi(data);

    yield put({
      type: CommentActionType.DELETE_COMMENT_SUCCESS,
      payload: data.payload,
    });
  } catch (error) {
    yield put({ type: CommentActionType.DELETE_COMMENT_ERROR, payload: null });
  }
}
export default function* watchGetCommentSaga() {
  yield takeLatest(CommentActionType.GET_COMMENT, getCommentSaga);
  // yield takeLatest(CommentActionType.CREATE_COMMENT, createCommentSaga)
  yield takeLatest(CommentActionType.DELETE_COMMENT, deleteCommentSaga);
}
