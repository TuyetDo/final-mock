import { call, put, take } from "redux-saga/effects";
import { GetPopularTags } from "../../configs/api/handleCallApi";
import { TagsActionType } from "../../model/actionType";
import { loadingData, loadingDataError, loadingDataSuccess } from "../action/actionCreators/LoadingAction";
import { getTagsError, getTagsSuccess } from "../action/actionCreators/TagsActions";



export function* getTagsFlow() {
    while (true) {
        yield take(TagsActionType.GET_TAGS);
        yield put(loadingData())

        const response = yield call(() => {
            return GetPopularTags();
        });

        if (response) {
            yield put(getTagsSuccess(response));
            yield put(loadingDataSuccess())

        } else {
            yield put(getTagsError(response));
            yield put(loadingDataError())

        }

    }
}

