import { put, takeLatest } from "redux-saga/effects";
import { getSlugArticle } from "../../configs/api/handleCallApi";
import { ArticleActionType } from "../../model/actionType";
import { getArticleSlug, getArticleSlugSuccess } from "../action/actionCreators/ArticleActions";

function* getSlugArticleSaga(slug: ReturnType<typeof getArticleSlug>) {

  try {
    const data = yield getSlugArticle(slug.payload);
    yield put(getArticleSlugSuccess(data))
    // yeild put(getArticleSlugSuccess(data))
    /*
      getSlugArticle(slug)
      .then(response => {
        put(getArticleSlugSuccess(response.data))
      })
    */

  } catch (error) {
    yield put({type: ArticleActionType.GET_ARTICLE_SLUG_ERROR, payload: error})
  }
}

export default function* watchGetSlugArticle(){
  yield takeLatest(ArticleActionType.GET_ARTICLE_SLUG, getSlugArticleSaga)
}
