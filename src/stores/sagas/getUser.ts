import { call, put, take } from "redux-saga/effects";
import { GetUser } from "../../configs/api/handleCallApi";
import { UserActionType } from "../../model/actionType";
import {
  getUserSuccess,
  getUserError,
} from "../action/actionCreators/AuthActions";
import {
  loadingData,
  loadingDataSuccess,
  loadingDataError,
} from "../action/actionCreators/LoadingAction";

export function* getUserFlow() {
  while (true) {
    yield take(UserActionType.GET_USER);
    yield put(loadingData());

    const response = yield call(() => {
      return GetUser();
    });

    if (response) {
      yield put(getUserSuccess(response.user));
      yield put(loadingDataSuccess());
    } else {
      yield put(getUserError());
      yield put(loadingDataError());
    }
  }
}
