import {
  loadingDataSuccess,
  loadingDataError,
} from "./../action/actionCreators/LoadingAction";

import {
  call,
  delay,
  fork,
  put,
  race,
  take,
  takeLatest,
} from "redux-saga/effects";
import {
  GetArticles,
  GetProfile,
  GetFeed,
  favoriteArticleApi,
  DeleteArticleSlug,
  followArticleApi,
} from "../../configs/api/handleCallApi";

import { ArticleActionType, ProfileActionType } from "../../model/actionType";
import {
  cancelDeleteArticle,
  deleteArticleConfirm,
  favoriteArticle,
  favoriteArticleError,
  favoriteArticleSuccess,
  followArticle,
  followArticleError,
  followArticleSuccess,
  getArticlesError,
  getArticlesSuccess,
} from "../action/actionCreators/ArticleActions";
import {
  getArticleTrendingError,
  getArticleTrendingSuccess,
} from "../action/actionCreators/ArticleTrendingAction";
import { loadingData } from "../action/actionCreators/LoadingAction";
import {
  getPageArticlesError,
  getPageArticlesSuccess,
} from "../action/actionCreators/PaginationAction";
import {
  getProfileError,
  getProfileSuccess,
} from "../action/actionCreators/ProfileAction";
import {
  getArticleFeedError,
  getArticleFeedSuccess,
} from "../action/actionCreators/FeedActions";

//get article when load page
export function* getArticleFlow() {
  while (true) {
    yield take(ArticleActionType.GET_ARTICLE);
    yield put(loadingData());
    const response = yield call(() => {
      return GetArticles();
    });

    if (response) {
      yield put(getArticlesSuccess(response));
      yield put(loadingDataSuccess());
    } else {
      yield put(getArticlesError(response));
      yield put(loadingDataError());
    }
  }
}

//get trending article
function* GetTrending(payload: any) {
  try {
    const trendingReponse = yield call(() => {
      return GetArticles(payload);
    });
    yield put(getArticleTrendingSuccess(trendingReponse));
    yield put(loadingDataSuccess());
  } catch (error) {
    yield put(getArticleTrendingError(error));
    yield put(loadingDataError());
  }
}

export function* getArticleTrendingFlow() {
  while (true) {
    const { payload } = yield take(ArticleActionType.GET_ARTICLE_TRENDING);
    // yield put(loadingData())
    yield fork(GetTrending, payload);
  }
}

//get article when change page pagination
function* GetArticlesPage(payload: any) {
  try {
    const pageArticles = yield call(() => {
      return GetArticles(undefined, undefined, undefined, undefined, payload);
    });
    yield put(getPageArticlesSuccess(pageArticles));
  } catch (error) {
    yield put(getPageArticlesError(error));
  }
}

export function* getArticlePaginationFlow() {
  while (true) {
    const { payload } = yield take(ArticleActionType.GET_ARTICLE_PAGINATION);
    yield fork(GetArticlesPage, payload);
  }
}

// get article by author

export function* getArticlesProfileFlow() {
  while (true) {
    const { payload } = yield take(ProfileActionType.GET_PROFILE);
    const { profile } = yield call(() => {
      return GetProfile(payload);
    });
    const { articles } = yield call(() => {
      return GetArticles(undefined, payload);
    });
    yield put(loadingData());

    if (profile && articles) {
      yield put(getArticlesSuccess(articles));
      yield put(getProfileSuccess(profile));
      yield put(loadingDataSuccess());
    } else {
      yield put(getArticlesError(articles));
      yield put(getProfileError(profile));
      yield put(loadingDataError());
    }
  }
}

// get Feed by user login (authorization required)

export function* getArticleFeedFlow() {
  while (true) {
    yield take(ArticleActionType.GET_ARTICLE_FEED);
    yield put(loadingData());
    const response = yield call(() => {
      return GetFeed();
    });

    if (response) {
      yield put(getArticleFeedSuccess(response));
      yield put(loadingDataSuccess());
    } else {
      yield put(getArticleFeedError(response));
      yield put(loadingDataError());
    }
  }
}

export function* createFavoriteArticleFlow(
  param: ReturnType<typeof favoriteArticle>
) {
  try {
    const payload = yield favoriteArticleApi(param.payload);
    yield put(favoriteArticleSuccess(payload));
  } catch (error) {
    yield put(favoriteArticleError(error));
  }
}

// follow
export function* createFollowArticle(param: ReturnType<typeof followArticle>) {
  try {
    const payload = yield followArticleApi(param.payload);

    yield put(followArticleSuccess(payload));
  } catch (error) {
    yield put(followArticleError(error));
  }
}

export function* deleteArticleFlow() {
  while (true) {
    const { payload } = yield take(ArticleActionType.DELETE_ARTICLE);
    const { delayNotify, deleteNow, cancelDelete } = yield race({
      delayNotify: delay(5000),
      deleteNow: take(ArticleActionType.DELETE_ARTICLE_NOW),
      cancelDelete: take(ArticleActionType.CANCEL_DELETE_ARTICLE),
    });
    if (delayNotify || deleteNow) {
      DeleteArticleSlug(payload.slug);
      yield put(deleteArticleConfirm());
    } else if (cancelDelete) {
      yield put(cancelDeleteArticle());
    }
  }
}

export function* createFavoriteArticleFlowSaga() {
  yield takeLatest(
    ArticleActionType.CREATE_FAVORITE_ARTICLE,
    createFavoriteArticleFlow
  );
}

export function* createFollowArticleSaga() {
  yield takeLatest(
    ArticleActionType.CREATE_FOLLOW_ARTICLE,
    createFollowArticle
  );
}
