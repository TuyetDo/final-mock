import { all } from "redux-saga/effects";
import { getUserFlow } from "./getUser";
import { loginFlow, registerFlow } from "./auth";

import {
  createFavoriteArticleFlowSaga,
  createFollowArticleSaga,
  deleteArticleFlow,
  getArticleFeedFlow,
  getArticleFlow,
  getArticlePaginationFlow,
  getArticlesProfileFlow,
  getArticleTrendingFlow,
} from "./articlesSaga";

import { updateUserFlow } from "./updateUser";
import { getTagsFlow } from "./tagsSaga";
import watchGetSlugArticle from "./getSlugArticle";
import watchGetCommentSaga from "./commentSaga";

export function* rootSaga() {
  yield all([
    getUserFlow(),
    loginFlow(),
    registerFlow(),
    getArticleFlow(),
    getArticleTrendingFlow(),
    getTagsFlow(),
    getArticlePaginationFlow(),
    updateUserFlow(),
    getArticlesProfileFlow(),
    watchGetSlugArticle(),
    watchGetCommentSaga(),
    createFavoriteArticleFlowSaga(),
    createFollowArticleSaga(),
    getArticleFeedFlow(),
    deleteArticleFlow(),
  ]);
}
