import React, { useState } from "react";
import { createStyles, makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Avatar from "@material-ui/core/Avatar";
import Typography from "@material-ui/core/Typography";
import Skeleton from "@material-ui/lab/Skeleton";
import { useSelector } from "react-redux";
import { RootState } from "../../stores/reducers";
import { Article } from "../../model/articleType";
import { images } from "../../assets/images/images";
import { IconButton } from "@material-ui/core";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import FavoriteIcon from "@material-ui/icons/Favorite";
import FavoriteBorderOutlinedIcon from "@material-ui/icons/FavoriteBorderOutlined";
import manImage from "../../assets/images/man.jpg";
import { motion } from "framer-motion";
import { Link } from "react-router-dom";

interface Prop {
  articles: Article;
  handleProfile: (user: string) => void;
}

export default function ArticleItem({ articles, handleProfile }: Prop) {
  let randomNum = Math.floor(Math.random() * 20);

  const isloading = useSelector((state: RootState) => state.isLoading);

  return (
    <div>
      {isloading ? (
        <Media isloading handleProfile={handleProfile} />
      ) : (
        <div className="d-flex justify-content-between">
          <Media
            book={articles}
            randomNum={randomNum}
            handleProfile={handleProfile}
          />
        </div>
      )}
    </div>
  );
}

const useStyles = makeStyles(() =>
  createStyles({
    card: {
      width: "100%",
      height: "450px",
      boxShadow: "none",
      border: "1px solid rgba(66,66,66,0.1)",
      borderRadius: 0,
    },
    small: {
      fontSize: "16px",
    },
    media: {
      width: "100%",
      height: 195,
    },
  })
);

interface MediaProps {
  isloading?: boolean;
  book?: Article;
  randomNum?: number;
  handleProfile: (user: string) => void;
}

const CheckURL = (url: any) => {
  const a = url.match(/\.(jpeg|jpg|gif|png)$/);
  return Boolean(a);
};

const transition = { duration: 0.6, ease: [0.43, 0.13, 0.23, 0.96] };

const Media = React.memo(function Media(props: MediaProps) {
  const { isloading, book, randomNum, handleProfile } = props;

  const [like, setLike] = useState<boolean>(false);
  const classes = useStyles();

  return (
    <Card className={classes.card}>
      <CardHeader
        avatar={
          isloading ? (
            <Skeleton
              animation="wave"
              variant="circle"
              width={40}
              height={40}
            />
          ) : (
            <Avatar
              alt="Author"
              src={CheckURL(book?.author.image) ? book?.author.image : manImage}
            />
          )
        }
        title={
          isloading ? (
            <Skeleton
              animation="wave"
              height={10}
              width="80%"
              style={{ marginBottom: 6 }}
            />
          ) : (
            <div className="d-flex justify-content-start mt-4">
              <h5>
                <Link to={`/articles/${book && book.slug}`}>
                  {book ? book.title : ""}
                </Link>
              </h5>
            </div>
          )
        }
        subheader={
          isloading ? (
            <Skeleton animation="wave" height={10} width="40%" />
          ) : (
            <div className="d-flex justify-content-between">
              <Link to={`/profile/${book && book.author.username}`}>
                {book ? (
                  <b
                    style={{ cursor: "pointer" }}
                    onClick={() => handleProfile(book.author.username)}
                  >
                    By: <i>{book ? book.author.username : ""}</i>
                  </b>
                ) : (
                  <b style={{ cursor: "pointer" }}>
                    By: <i></i>
                  </b>
                )}
              </Link>
              <i>
                {book
                  ? new Intl.DateTimeFormat("vi-VN").format(
                      new Date(book.updatedAt)
                    )
                  : ""}
              </i>
            </div>
          )
        }
      />
      {isloading ? (
        <Skeleton animation="wave" variant="rect" className={classes.media} />
      ) : (
        <motion.div whileHover={{ scale: 1.05 }} transition={transition}>
          <CardMedia
            className={classes.media}
            image={randomNum ? images[randomNum] : images[0]}
            title="book"
          />
        </motion.div>
      )}
      <CardContent>
        {isloading ? (
          <React.Fragment>
            <Skeleton
              animation="wave"
              height={10}
              style={{ marginBottom: 6 }}
            />
            <Skeleton animation="wave" height={10} width="80%" />
          </React.Fragment>
        ) : (
          <div className="d-flex justify-content-start overflow-auto">
            <Typography variant="body2" color="textSecondary" component="pre">
              <Link to={`/articles/${book && book.slug}`}>
                {book ? book.body : ""}
              </Link>
            </Typography>
          </div>
        )}
      </CardContent>
      <div className="d-flex justify-content-between">
        <IconButton
          aria-label="add to favorites"
          onClick={() => {
            setLike(!like);
          }}
        >
          {like ? (
            <FavoriteIcon color="secondary" />
          ) : (
            <FavoriteBorderOutlinedIcon />
          )}
        </IconButton>

        <IconButton aria-label="show more">
          <div className={classes.small}>
            <Link to={`/articles/${book && book.slug}`}>
              <small>Read more...</small>
            </Link>
          </div>
          <ExpandMoreIcon color="primary" />
        </IconButton>
      </div>
      <div className="text-left ml-2 text-wrap text-truncate">
        <i>
          <small>Hashtag:</small>
          {book
            ? book.tagList
                .filter((tag) => {
                  return tag.match(/^[\w&.]+$/);
                })
                .map((tag) => (
                  <small key={tag} className="text-primary ml-1">
                    #{tag}
                  </small>
                ))
            : ""}
        </i>
      </div>
    </Card>
  );
});
