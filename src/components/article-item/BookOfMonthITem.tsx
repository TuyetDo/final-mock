import React from "react";
import { createStyles, makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Avatar from "@material-ui/core/Avatar";
import Typography from "@material-ui/core/Typography";
import Skeleton from "@material-ui/lab/Skeleton";
import { useSelector } from "react-redux";
import { RootState } from "../../stores/reducers";
import manImage from "../../assets/images/man.jpg";
import { Article } from "../../model/articleType";
// import { images } from "../../assets/images/images";
import adImage from "../../assets/images/ad.gif";
// import { getProfile } from "../../stores/action/actionCreators/ProfileAction";
import { motion } from "framer-motion";

export default function BookOfMonthItem() {
  const isloading = useSelector((state: RootState) => state.isLoading);
  const bookOfMonth = useSelector((state: RootState) => state.articles);

  let book;
  if (bookOfMonth) {
    book = bookOfMonth.articles?.filter((article: Article) => {
      return bookOfMonth.articles?.indexOf(article) === 10;
    });
  }
  return <div>{isloading ? <Media loading /> : <Media book={book} />}</div>;
}

const useStyles = makeStyles(() =>
  createStyles({
    card: {
      width: "100%",
      height: "auto",
      boxShadow: "none",
      border: "1px solid rgba(66,66,66,0.1)",
      borderRadius: 0,
    },
    media: {
      height: 190,
    },
  })
);

interface MediaProps {
  loading?: boolean;
  book?: Article[];
}

const Media = React.memo(function Media(props: MediaProps) {
  // let randomNum = Math.floor(Math.random() * 20);
  const { loading = false } = props;
  // const dispatch = useDispatch();
  const classes = useStyles();
  // let author: string;
  // if (book) {
  //   const user = book[0].author.username;
  //   author =
  //     user.toLowerCase().indexOf("@") > 0
  //       ? user.toLowerCase().slice(0, user.indexOf("@"))
  //       : user.toLowerCase();
  // }
  // const handeClick = (author: string) => {
  //   dispatch(getProfile(author));
  // };
  const transition = { duration: 0.6, ease: [0.43, 0.13, 0.23, 0.96] };

  return (
    <Card className={classes.card}>
      <div style={{ cursor: "pointer" }}>
        <CardHeader
          avatar={
            loading ? (
              <Skeleton
                animation="wave"
                variant="circle"
                width={40}
                height={40}
              />
            ) : (
              <Avatar alt="Author" src={manImage} />
            )
          }
          title={
            loading ? (
              <Skeleton
                animation="wave"
                height={10}
                width="80%"
                style={{ marginBottom: 6 }}
              />
            ) : (
              // <h6>{book ? book[0]?.title : ""}</h6>
              <h6>Tuyển người yêu</h6>
            )
          }
          subheader={
            loading ? (
              <Skeleton animation="wave" height={10} width="40%" />
            ) : (
              <i>
                {/* {book
                ? new Intl.DateTimeFormat("vi-VN").format(
                    new Date(book[0].updatedAt)
                  )
                : ""} */}
                Trứng rán càn mỡ bắp cần bơ, yêu không cần cớ cần cậu cơ.
              </i>
            )
          }
        />
      </div>
      {loading ? (
        <Skeleton animation="wave" variant="rect" className={classes.media} />
      ) : (
        <motion.div whileHover={{ scale: 1.2 }} transition={transition}>
          <CardMedia
            className={classes.media}
            // image={images[randomNum]}
            image={adImage}
            title="book"
          />
        </motion.div>
      )}
      <CardContent>
        {loading ? (
          <React.Fragment>
            <Skeleton
              animation="wave"
              height={10}
              style={{ marginBottom: 6 }}
            />
            <Skeleton animation="wave" height={10} width="80%" />
          </React.Fragment>
        ) : (
          <Typography variant="body2" color="textSecondary" component="p">
            {/* {book ? book[0].body : ""} */}
            Liên hệ để biết thêm chi tiết:{" "}
            <i style={{ color: "red" }}>0909.999.999</i>
          </Typography>
        )}
      </CardContent>
    </Card>
  );
});
