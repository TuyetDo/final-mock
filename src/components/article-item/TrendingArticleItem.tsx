import React from "react";
import { Link } from "react-router-dom";
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import FavoriteIcon from "@material-ui/icons/Favorite";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import { Article } from "../../model/articleType";
import { images } from "../../assets/images/images";
import { useSelector } from "react-redux";
import { RootState } from "../../stores/reducers";
import { Skeleton } from "@material-ui/lab";
import styled from "styled-components";
import Carousel from "react-material-ui-carousel";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      maxWidth: "100%",
      boxShadow: "none !important",
      position: "relative",
      border: "1px solid rgba(66,66,66,0.1)",
      borderRadius: 0,
    },
    media: {
      height: 0,
      paddingTop: "56.25%", // 16:9
      boxShadow: "none !important",
    },
    [theme.breakpoints.down("md")]: {
      fontSize: "10px",
    },

    avatar: {
      height: 30,
      backgroundColor: "transparent",
      color: "#3f51b5",
      borderRadius: 0,
      fontSize: 15,
      fontWeight: "bold",
      textTransform: "capitalize",
      cursor: "pointer",
    },
    small: {
      fontSize: "14px",
      marginRight: "8px",
    },
  })
);

interface Props {
  trendingArticle?: Article[];
  handleProfile: (user: string) => void;
}

const StyledCardHeader = styled(CardHeader)`
  .title {
    text-align: center;
    @media (max-width: 768px) {
      h1 {
        font-size: 24px;
      }
    }
  }
`;

export default function ArticleItem({ trendingArticle, handleProfile }: Props) {
  const classes = useStyles();
  const isloading = useSelector((state: RootState) => state.isLoading);

  let randomNum;

  let authorName: string;

  return (
    <div>
      {isloading ? (
        <div>
          <Skeleton width="70%" />
          <Skeleton variant="rect" width={"100%"} height={350} />
          <Skeleton />
          <Skeleton width="60%" />
        </div>
      ) : (
        <div>
          <Carousel
            autoPlay={true}
            animation={"slide"}
            interval={2500}
            swipe={false}
          >
            {trendingArticle?.map((article, index) => {
              return (
                <Card className={classes.root} key={index}>
                  {(randomNum = Math.floor(Math.random() * 20)) < 21
                    ? ""
                    : "loading..."}
                  <CardMedia
                    className={classes.media}
                    image={images[randomNum]}
                    title="trending"
                  />
                  <div>
                    <StyledCardHeader
                      title={
                        <h5 className="title">
                          <Link
                            to={`/articles/${trendingArticle && article.slug}`}
                          >
                            {trendingArticle ? article.title : ""}
                          </Link>
                        </h5>
                      }
                    />
                    <CardContent>
                      <div className="author d-flex justify-content-between mb-3">
                        {
                          (authorName =
                            article.author.username.toLowerCase().indexOf("@") >
                            0
                              ? article.author.username
                                  .toLowerCase()
                                  .slice(
                                    0,
                                    article.author.username.indexOf("@")
                                  )
                              : article.author.username.toLowerCase())
                        }
                        <Link to={`/profile/${authorName}`}>
                          <Typography
                            aria-label="recipe"
                            className={classes.avatar}
                            onClick={() => handleProfile(authorName)}
                          >
                            By: {article.author.username}
                          </Typography>
                        </Link>
                        <i className="right-0">
                          {new Intl.DateTimeFormat("vi-VN").format(
                            new Date(article.updatedAt)
                          )}
                        </i>
                      </div>
                      <Typography
                        variant="body2"
                        color="textSecondary"
                        component="p"
                        className="text-left"
                      >
                        {article.body}
                      </Typography>
                    </CardContent>
                    <CardActions
                      disableSpacing
                      className="d-flex justify-content-between"
                    >
                      <div>
                        <IconButton aria-label="add to favorites">
                          <small className={classes.small}>
                            {article.favoritesCount}
                          </small>
                          <FavoriteIcon color="secondary" />
                        </IconButton>
                      </div>
                      <div>
                        <IconButton aria-label="show more">
                          <Link to={`/articles/${article.slug}`}>
                            <small className={classes.small}>
                              Read more...
                            </small>
                          </Link>
                          <ExpandMoreIcon color="primary" />
                        </IconButton>
                      </div>
                    </CardActions>
                  </div>

                  <div className="text-left ml-2 mb-2">
                    Hashtag:
                    {article.tagList
                      .filter((tag) => tag.match(/^[\w&.]+$/))
                      .map((tag) => (
                        <small key={tag} className="text-primary ml-1">
                          #{tag}
                        </small>
                      ))}
                  </div>
                </Card>
              );
            })}
          </Carousel>
        </div>
      )}
    </div>
  );
}
