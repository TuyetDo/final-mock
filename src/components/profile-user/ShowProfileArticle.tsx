import { Avatar, Button } from "@material-ui/core";
import SettingsIcon from "@material-ui/icons/Settings";
import AddIcon from "@material-ui/icons/Add";
import React, { useState } from "react";
import { Link } from "react-router-dom";
import { Profile } from "../../model/profileType";
import { userAuthResponse } from "../../model/userType";
import CircularProgress from "@material-ui/core/CircularProgress";

interface Props {
  lastProfile: Profile;
  user: userAuthResponse;
}
const ShowProfileArticle = ({ lastProfile, user }: Props) => {
  const checkedAuthor =
    lastProfile && user && lastProfile.username === user.username
      ? true
      : false;

  const [following, setFollowing] = useState(
    lastProfile && lastProfile.following
  );
  const onFollow = () => {
    // const followingArticle =
  };
  return (
    <div>
      {(lastProfile && lastProfile.username) === undefined ? (
        <CircularProgress className="text-primary" />
      ) : (
        <div>
          <Avatar src={`${lastProfile && lastProfile.image}`} />
          <h4>{`${lastProfile && lastProfile.username}`}</h4>
          <p>{`${lastProfile && lastProfile.bio ? lastProfile.bio : ""}`}</p>
        </div>
      )}

      <div>
        {checkedAuthor ? (
          <Button>
            <Link to={`/setting`}>
              <SettingsIcon /> Edit profile setting
            </Link>
          </Button>
        ) : (lastProfile && lastProfile.username) === undefined ? (
          <CircularProgress className="text-primary" />
        ) : (
          <Button
            variant="outlined"
            color="primary"
            onClick={() => onFollow()}
            style={{ margin: "10px 30px" }}
          >
            <AddIcon />
            {following
              ? `Unfollow ${lastProfile && lastProfile.username}`
              : `Follow ${lastProfile && lastProfile.username}`}
          </Button>
        )}
      </div>
    </div>
  );
};

export default ShowProfileArticle;
