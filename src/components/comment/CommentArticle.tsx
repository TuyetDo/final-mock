import {
  makeStyles,
  List,
  ListItem,
  Divider,
  Button,
  SwipeableDrawer,
  TextField,
  createStyles,
  Theme,
  Avatar,
} from "@material-ui/core";
import React, { useEffect, useState } from "react";
import clsx from "clsx";
import RecipeReviewCard from "../card/CardComment";
import { useDispatch, useSelector } from "react-redux";
import {
  deleteComment,
  getComments,
} from "../../stores/action/actionCreators/CommentActions";
import { RootState } from "../../stores/reducers";
import { CommentCreate, Comments } from "../../model/articleType";
import { findIdComment } from "../../configs/delete comment/findIdComment";
import { createCommentApi } from "../../configs/api/handleCallApi";
import manImage from "../../assets/images/man.jpg";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    list: {
      width: 400,
    },
    fullList: {
      width: "auto",
    },
    alert: {
      padding: theme.spacing(1),
      margin: theme.spacing(1, 0),
      border: "1px solid",
    },
    styledShowResponse: {
      backgroundColor: "white",
      borderRadius: "5px",
      boxShadow: "10px #9b989c",
      marginTop: theme.spacing(4),
    },
    customInput: {
      marginTop: theme.spacing(4),
    },
    avatar: {
      display: "flex",
      marginBottom: "20px",
    },
    styledForm: {
      padding: "15px",
    },
    user: {
      marginLeft: "7px",
      marginTop: "4px",
    },
    buttonStyled: {
      marginTop: "10px",
    },
  })
);

type Anchor = "bottom" | "right";

interface Props {
  parameter: string;
  openProp: boolean;
  onChangeOpen: (value?: boolean) => void;
  username: string;
  img: string;
}
export const CommentArticle = ({
  parameter,
  openProp,
  onChangeOpen,
  username,
  img,
}: Props) => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const comments = useSelector((state: RootState) => state.comments.comments);
  let [listComment, setListComment] = useState<Comments[]>(comments);

  const [state, setState] = React.useState({
    bottom: false,
    right: false,
  });
  useEffect(() => {}, [listComment]);
  const [valueInput, setValueInput] = useState("");
  const [showResponse, setShowResponse] = useState(true);

  useEffect(() => {
    dispatch(getComments(parameter));
  }, [dispatch, parameter]);

  const handleRemove = (id: number) => {
    const slug = parameter;
    let value = 0;
    if (listComment.length > 0) {
      value = findIdComment(listComment, id);
    } else {
      value = findIdComment(comments, id);
    }
    // const value = findIdComment(listComment, id);
    const commentId = id;

    dispatch(deleteComment(slug, commentId));
    const newListComment = listComment
      .slice(0, value)
      .concat(listComment.slice(value + 1));

    setListComment(newListComment.reverse());
  };

  const toggleDrawer = (anchor: Anchor, open: boolean) => (
    event: React.KeyboardEvent | React.MouseEvent
  ) => {
    if (
      event &&
      event.type === "keydown" &&
      ((event as React.KeyboardEvent).key === "Tab" ||
        (event as React.KeyboardEvent).key === "Shift")
    ) {
      // onChangeOpen(true);
      return;
    }

    setState({ ...state, [anchor]: open });
    onChangeOpen(open);
  };

  const formResponse = () => (
    <form
      className={classes.styledForm}
      onSubmit={(event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        const data: CommentCreate = {
          comment: {
            comment: {
              body: valueInput,
            },
          },
          slug: parameter,
        };
        const dataCreateComment = createCommentApi(data);
        dataCreateComment
          .then((response) => {
            setListComment([response, ...listComment]);
          })
          .catch((error) => {
            return error.message;
          });
        setValueInput("");
      }}
    >
      <div className={classes.avatar}>
        <Avatar alt={username} src={manImage} />
        <p className={classes.user}>{username}</p>
      </div>
      <TextField
        placeholder="What are your throught"
        fullWidth
        value={valueInput}
        id={classes.customInput}
        variant="outlined"
        onChange={(event) => setValueInput(event?.target.value)}
        autoFocus={true}
      />
      <div className={classes.buttonStyled}>
        <Button variant="contained" color="primary" type="submit">
          Response
        </Button>
      </div>
    </form>
  );

  const list = (anchor: Anchor) => {
    return (
      <div
        className={clsx(classes.list, {
          [classes.fullList]: anchor === "bottom",
        })}
        role="presentation"
        onClick={toggleDrawer(anchor, openProp)}
        onKeyDown={toggleDrawer(anchor, openProp)}
      >
        <div className={classes.styledShowResponse}>
          {showResponse ? (
            <div className={classes.styledForm}>
              <TextField
                id={classes.customInput}
                fullWidth
                placeholder="What are your throught"
                // variant="outlined"
                onClick={() => setShowResponse(!showResponse)}
              />
            </div>
          ) : (
            formResponse()
          )}
        </div>

        <List>
          {/* map list comment */}
          {listComment && listComment.length > 0
            ? listComment.map((item: Comments) => (
                <ListItem divider key={item.id}>
                  <RecipeReviewCard item={item} onRemove={handleRemove} />
                </ListItem>
              ))
            : null}
        </List>
        <Divider />
      </div>
    );
  };

  return (
    <div>
      <React.Fragment>
        <SwipeableDrawer
          anchor={"right"}
          open={openProp}
          onClose={toggleDrawer("right", false)}
          onOpen={toggleDrawer("right", openProp)}
        >
          <h3>{`Response (${listComment && listComment.length})`}</h3>
          {list("right")}
        </SwipeableDrawer>
      </React.Fragment>
    </div>
  );
};
