import React from "react";
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import { Article } from "../../model/articleType";
import AtircleItem from "../article-item/AtircleItem";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
      marginBottom: "32px",
    },
    paper: {
      padding: theme.spacing(2),
      textAlign: "center",
      color: theme.palette.text.secondary,
    },
  })
);

interface ArticlesProps {
  articles: Article[];
  handleProfile: (user: string) => void;
}

export default function ArticlesList({
  articles,
  handleProfile,
}: ArticlesProps) {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Grid container spacing={3}>
        {articles?.map((articles) => {
          return (
            <Grid item lg={4} sm={6} xs={12} key={articles.slug}>
              <Paper className={classes.paper}>
                <AtircleItem
                  articles={articles}
                  handleProfile={handleProfile}
                />
              </Paper>
            </Grid>
          );
        })}
      </Grid>
    </div>
  );
}
