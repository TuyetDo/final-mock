import styled from "styled-components";


export const StyledHeader = styled.div`
  color: white;
  background-color: #041c2c;
  display:flex;
  height: 80px;
  justify-content: space-between;
  align-items: center;
  .logo {
    cursor: pointer;
    margin-left: 120px !important;
  }
  .navright{
    margin-right: 100px !important;
  }
  font-size: 17px !important;
  @media (max-width: 768px) {
    font-size: 13px !important;
    .logo {
      margin-left: 32px !important;
    }
    .navright{
      margin-right: 32px !important;
    }
  }
  li {
    margin-right: 10px;
  }
  
  .navbar-styled {
    display: flex;
    flex-direction: row;
    justify-content: space-around;
    .link-styled {
      color: white !important;
    }
  }

`
