import { AppBar } from "@material-ui/core";
import CircularProgress from "@material-ui/core/CircularProgress";
import React, { Fragment } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useHistory } from "react-router-dom";
import { logout } from "../../stores/action/actionCreators/AuthActions";
import { getProfile } from "../../stores/action/actionCreators/ProfileAction";
import { RootState } from "../../stores/reducers";
import DropdownComponent from "../DropdownComponent";
import { StyledHeader } from "./StyledHeader";

export interface OptionsDropdown {
  optionName: string;
  action: string;
  icon: React.DetailedHTMLProps<React.HTMLAttributes<HTMLElement>, HTMLElement>;
}

const Header = () => {
  const isLogin = Boolean(localStorage.getItem("token"));
  const currentUser = useSelector((state: RootState) => state.user);
  const isLoading = useSelector((state: RootState) => state.isLoading);
  const history = useHistory();
  const dispatch = useDispatch();
  const options: OptionsDropdown[] = [
    {
      optionName: "My Profile",
      action: "profile",
      icon: <i className="fas fa-user pr-2"></i>,
    },
    {
      optionName: "Setting",
      action: "setting",
      icon: <i className="fas fa-cog pr-2"></i>,
    },
    {
      optionName: "Logout",
      action: "logout",
      icon: <i className="fas fa-sign-out-alt pr-2"></i>,
    },
  ];

  const handleClick = (action: string) => {
    switch (action) {
      case "profile":
        dispatch(getProfile(currentUser.username));
        history.push(`/profile/${currentUser.username}`);

        break;
      case "setting":
        history.push(`/setting`);
        break;
      case "logout":
        const action = logout();
        dispatch(action);
        history.push("/");
        break;
      default:
        break;
    }
  };

  return (
    <AppBar>
      <StyledHeader>
        <Link to="/">
          <h3 className="logo">Book Lover</h3>
        </Link>
        <nav className="navbar navbar-expand-lg navbar-light">
          <div className="navbar-collapse" id="navbarNav">
            <ul className="navbar-nav navbar-styled">
              <li className="nav-item active">
                <Link className="nav-link link-styled" to="/">
                  Home <span className="sr-only">(current)</span>
                </Link>
              </li>
              {!isLogin ? (
                <Fragment>
                  <li className="nav-item">
                    <Link className="nav-link link-styled" to="/login">
                      Sign In
                    </Link>
                  </li>
                  <li className="nav-item">
                    <Link
                      className="nav-link link-styled navright"
                      to="/register"
                    >
                      Sign Up
                    </Link>
                  </li>
                </Fragment>
              ) : (
                <Fragment>
                  <li className="nav-item">
                    <Link className="nav-link link-styled" to="/editor">
                      <i className="far fa-edit"></i> New Article
                    </Link>
                  </li>
                  {isLoading ? (
                    <CircularProgress className="text-white" />
                  ) : (
                    currentUser.username && (
                      <li>
                        <DropdownComponent
                          labelName={currentUser.username}
                          options={options}
                          onClick={handleClick}
                        />
                      </li>
                    )
                  )}
                </Fragment>
              )}
            </ul>
          </div>
        </nav>
      </StyledHeader>
    </AppBar>
  );
};

export default Header;
