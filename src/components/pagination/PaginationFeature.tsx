import React from "react";
import Pagination from "@material-ui/lab/Pagination";
import { ScrollTop } from "../BackToFeed";

interface Prop {
  total?: number;
  handlePage: (page: number) => void;
}
const PaginationFeature = ({ total, handlePage }: Prop) => {
  return (
    <div className="mb-5 d-flex justify-content-center">
      <ScrollTop>
        <Pagination
          count={total ? total : 50}
          color="primary"
          defaultPage={1}
          onChange={(e, page) => {
            handlePage(page);
          }}
        />
      </ScrollTop>
    </div>
  );
};

export default PaginationFeature;
