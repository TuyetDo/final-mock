import {
  createStyles,
  FormControl,
  FormHelperText,
  makeStyles,
  TextField,
  Theme,
} from "@material-ui/core";
import { FieldProps } from "formik";
import React, { useState } from "react";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: "flex",
      flexWrap: "wrap",
    },
    textField: {
      marginLeft: theme.spacing(1),
      marginRight: theme.spacing(1),
      width: "50ch",
    },
  })
);

interface Props {
  propsField: FieldProps;
  labelName: string;
  placeholder?: string;
  onAddTag: (newTagArray: string[]) => void;
  oldTagArray?: string[];
  newTagArray: string[];
}

const FormControlNewArticle = ({
  propsField,
  labelName,
  placeholder,
  onAddTag,
  oldTagArray,
  newTagArray,
}: Props) => {
  const { field, form, meta } = propsField;
  const [valueInput, setValueInput] = useState("");

  const classes = useStyles();

  const regEx = /^[a-zA-Z0-9]+$/;

  const handleOnChange = (value: string) => {
    setValueInput(value);
    form.setFieldTouched("tagList", true);

    if (value.includes("\n") && onAddTag) {
      const valueCutEnter = value.slice(0, -1);
      if (
        valueCutEnter.match(regEx) &&
        !newTagArray.includes(valueCutEnter) &&
        oldTagArray &&
        !oldTagArray.includes(valueCutEnter)
      ) {
        let tagConcat = [...newTagArray];
        if (!newTagArray.includes(oldTagArray[0])) {
          tagConcat = newTagArray.concat(oldTagArray);
        }
        onAddTag([...tagConcat, valueCutEnter]);
        form.setFieldError("tagList", undefined);
        setValueInput("");
      } else if (
        valueCutEnter.match(regEx) &&
        !newTagArray.includes(valueCutEnter)
      ) {
        onAddTag([...newTagArray, valueCutEnter]);
        form.setFieldError("tagList", undefined);
        setValueInput("");
      } else if (newTagArray.includes(valueCutEnter)) {
        form.setFieldError("tagList", "Tag cannot be duplicated ");
        setValueInput("");
      } else {
        form.setFieldError("tagList", "Tag is not valid");
        setValueInput("");
      }
    }
  };
  return (
    <div>
      <FormControl className={classes.root} error>
        <TextField
          {...field}
          label={labelName}
          placeholder={placeholder ? placeholder : labelName}
          margin="normal"
          variant="outlined"
          multiline
          value={valueInput}
          onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
            handleOnChange(event.target.value)
          }
          className={classes.textField}
        />
        {meta.touched && meta.error && (
          <FormHelperText className={classes.textField}>
            {meta.error}
          </FormHelperText>
        )}
      </FormControl>
    </div>
  );
};

export default FormControlNewArticle;
