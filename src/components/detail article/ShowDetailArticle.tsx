import {
  Avatar,
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  CardMedia,
  createStyles,
  IconButton,
  makeStyles,
  Theme,
  Typography,
} from "@material-ui/core";
import React, { useState } from "react";
import CheckIcon from "@material-ui/icons/Check";
import FavoriteBorderIcon from "@material-ui/icons/FavoriteBorder";
import FavoriteIcon from "@material-ui/icons/Favorite";
import { useDispatch, useSelector } from "react-redux";
import {
  favoriteArticle,
  followArticle,
} from "../../stores/action/actionCreators/ArticleActions";
import { useParams } from "react-router-dom";
import { RootState } from "../../stores/reducers";
import manImage from "../../assets/images/man.jpg";
import { images } from "../../assets/images/images";
import CommentIcon from "@material-ui/icons/Comment";

interface Props {
  img: string;
  username: string;
  update: string;
  following: boolean;
  favorited: boolean;
  favoritesCount: number;
  // onFollow: () => void;
  body: string;
  tags: string[];
  title: string;
  showCountFavorite: () => number;
  handleComment: () => void;
  open: boolean;
}
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    large: {
      width: theme.spacing(7),
      height: theme.spacing(7),
    },
    root: {
      maxWidth: 800,
      marginBottom: 20,
      flexGrow: 1,
    },
    media: {
      height: 0,
      paddingTop: "56.25%", // 16:9
    },
    paper: {
      marginBottom: "25px",
      padding: theme.spacing(2),
      textAlign: "center",
      color: theme.palette.text.secondary,
    },
  })
);
const randomNum = Math.floor(Math.random() * 20);

export const ShowDetailArticle = ({
  username,
  update,
  following,
  favorited,
  tags,
  body,
  // onFollow,
  title,
  showCountFavorite,
  handleComment,
  open,
}: Props) => {
  const dispatch = useDispatch();
  let { slug } = useParams<{ slug: string }>();
  const favorite = useSelector((state: RootState) => state.favorited);
  const followed = useSelector((state: RootState) => state.followed);

  const classes = useStyles();

  const [checked, setChecked] = useState(
    favorite && favorite.article ? favorite.article.favorited : favorited
  );

  const [checkedFollow, setCheckedFollow] = useState(
    followed && followed.following ? followed.following : following
  );

  const handleFavorite = () => {
    dispatch(favoriteArticle(slug, checked));
    setChecked(!checked);
  };

  const onFollow = () => {
    dispatch(followArticle(username, checkedFollow, slug));
    setCheckedFollow(!checkedFollow);
  };

  return (
    <div>
      <Card className={classes.root}>
        <CardHeader
          avatar={<Avatar className={classes.large} src={manImage} />}
          action={
            <Button
              color="primary"
              onClick={() => onFollow()}
              style={{ margin: "9px 10px", width: " 145px" }}
            >
              {checkedFollow ? (
                <div>
                  <CheckIcon />
                  &nbsp;Following
                </div>
              ) : (
                ` Follow`
              )}
            </Button>
            // </IconButton>
          }
          title={username}
          // subheader={new Intl.DateTimeFormat("vi-VN").format(new Date(update))}
        />
        <CardMedia
          className={classes.media}
          image={images[randomNum]}
          title={title}
        />
        <CardContent>
          <Typography variant="body2" color="textSecondary" component="pre">
            {body}
          </Typography>
        </CardContent>
        <CardActions disableSpacing className="d-flex justify-content-between">
          <IconButton aria-label="add to favorites">
            {showCountFavorite()}
            <FavoriteIcon color="secondary" />
          </IconButton>
          <div>
            <Button color="secondary" onClick={() => handleFavorite()}>
              {checked ? (
                <div>
                  <FavoriteIcon color="secondary" />
                  &nbsp;Favorited
                </div>
              ) : (
                <div>
                  <FavoriteBorderIcon />
                  &nbsp;Favorite
                </div>
              )}
            </Button>
          </div>
          <Box>
            <Button
              onClick={() => {
                handleComment();
              }}
            >
              <CommentIcon />
            </Button>
          </Box>
        </CardActions>
        <div className="text-left ml-2 text-wrap text-truncate mb-3">
          <i>
            <small>Hashtag:</small>
            {tags
              .filter((tag) => {
                return tag.match(/^[\w&.]+$/);
              })
              .map((tag) => (
                <small key={tag} className="text-primary ml-1">
                  #{tag}
                </small>
              ))}
          </i>
        </div>
      </Card>
    </div>
  );
};
