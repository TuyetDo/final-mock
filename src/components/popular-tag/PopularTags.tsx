import { Chip, makeStyles } from "@material-ui/core";
import { Skeleton } from "@material-ui/lab";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getTags } from "../../stores/action/actionCreators/TagsActions";
import { RootState } from "../../stores/reducers";

const useStyles = makeStyles({
  root: {
    width: "100%",
    // marginLeft: "10%",
    // padding: "8px",
    border: "1px solid rgba(66,66,66,0.1)",
    borderRadius: 0,
  },
});
interface Props {
  handleClick: (tag: string) => void;
}

const PopularTags = ({ handleClick }: Props) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const isloading = useSelector((state: RootState) => state.isLoading);

  useEffect(() => {
    dispatch(getTags());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  const tags = useSelector((state: RootState) => state.tags);

  const newTags = tags?.filter((tag: string) => {
    return tag.match(/^[\w&.]+$/);
  });

  return (
    <div className={classes.root}>
      <h6 className="mt-2">POPULAR TAGS</h6>
      {isloading ? (
        <div>
          <Skeleton />
          <Skeleton />
          <Skeleton />
          <Skeleton />
        </div>
      ) : (
        <div>
          {newTags?.map((tag: string, index: number) => {
            return (
              <small key={index}>
                <Chip
                  label={tag}
                  onClick={() => handleClick(tag)}
                  color="primary"
                  variant="outlined"
                  className="m-1 text-capitalize"
                />
              </small>
            );
          })}
        </div>
      )}
    </div>
  );
};

export default PopularTags;
