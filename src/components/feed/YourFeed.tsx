import React from "react";
import { Article } from "../../model/articleType";
import ArticlesList from "../articles-list/ArticlesList";

interface ArticlesProps {
  articles: Article[];
  handleProfile: (user: string) => void;
}
const YourFeed = ({ articles, handleProfile }: ArticlesProps) => {
  return (
    <div>
      <ArticlesList articles={articles} handleProfile={handleProfile} />
    </div>
  );
};

export default YourFeed;
