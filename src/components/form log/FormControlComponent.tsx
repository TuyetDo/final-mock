import {
  createStyles,
  FormControl,
  FormHelperText,
  makeStyles,
  TextField,
  Theme,
} from "@material-ui/core";
import { Field, FieldProps } from "formik";
import React from "react";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: "flex",
      flexWrap: "wrap",
    },
    textField: {
      marginLeft: theme.spacing(1),
      marginRight: theme.spacing(1),
      width: "80vw",
    },
  })
);

interface Props {
  labelName: string;
  fieldName: string;
  typeName?: string;
  row?: number;
  placeholder?: string;
}

const FormControlComponent = ({
  labelName,
  fieldName,
  typeName,
  row,
  placeholder,
}: Props) => {
  const classes = useStyles();
  return (
    <Field name={fieldName}>
      {(propsField: FieldProps) => {
        const { field, meta } = propsField;
        return (
          <FormControl className={classes.root} error>
            <TextField
              {...field}
              label={labelName}
              placeholder={placeholder ? placeholder : labelName}
              margin="normal"
              variant="outlined"
              className={classes.textField}
              type={typeName ? typeName : "text"}
              multiline={row ? true : false}
              rows={row ? row : 1}
            />
            {meta.touched && meta.error && (
              <FormHelperText className={classes.textField}>
                {meta.error}
              </FormHelperText>
            )}
          </FormControl>
        );
      }}
    </Field>
  );
};

export default FormControlComponent;
