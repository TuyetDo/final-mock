import {
  Button,
  Checkbox,
  createStyles,
  FormHelperText,
  Grid,
  makeStyles,
} from "@material-ui/core";
import { Formik, FormikProps, FormikValues, Form } from "formik";
import React, { useState } from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import * as Yup from "yup";
import { userInputInfo } from "../../model/userType";
import { RootState } from "../../stores/reducers";
import FormControlComponent from "./FormControlComponent";

const useStyles = makeStyles(() =>
  createStyles({
    submitBtn: {
      display: "block",
      margin: "5px auto",
    },
    heightDiv: {
      marginTop: "140px",
    },
    marginLeftText: {
      margin: "0px 8px",
    },
    text: {
      textAlign: "center",
    },
  })
);

interface Props {
  onSubmitUser: (user: userInputInfo) => void;
  actionLogin: boolean;
}

// ---------------------------------Validate-----------------------------------
const LoginSchema = Yup.object().shape({
  email: Yup.string().email("Invalid email").required("Email is required"),
  password: Yup.string()
    .min(8, "Password too short!")
    .max(50, "Password too long!")
    .required("Password is required"),
});

const RegisterSchema = LoginSchema.shape({
  username: Yup.string()
    .min(1, "User name too short!")
    .max(50, "User name too long!")
    .required("User name is required"),
  confirmPassword: Yup.string()
    .oneOf([Yup.ref("password"), null], "Confirm password must equal password")
    .required("Confirm password is required"),
});

const FormLog = ({ onSubmitUser, actionLogin }: Props) => {
  const [showPassword, setShowPassword] = useState(false);
  const [notify, setNotify] = useState(false);
  const error = useSelector((state: RootState) => state.user.error);

  const handleShowPassword = () => {
    setShowPassword(!showPassword);
  };

  const classes = useStyles();

  return (
    <Grid className={classes.heightDiv} xs={12}>
      <div className="container-fluid">
        <h1 className="text-center">{actionLogin ? "SIGN IN" : "SIGN UP"}</h1>
        <div className="row d-flex justify-content-center">
          <Formik
            initialValues={{
              username: !actionLogin ? "" : undefined,
              email: "",
              password: "",
              confirmPassword: "",
            }}
            validationSchema={actionLogin ? LoginSchema : RegisterSchema}
            onSubmit={(values) => {
              const newUser: userInputInfo = {
                username: !actionLogin ? values.username : undefined,
                email: values.email,
                password: values.password,
              };
              onSubmitUser(newUser);
              setNotify(true);
            }}
          >
            {(propsFormik: FormikProps<FormikValues>) => {
              return (
                <Form>
                  {!actionLogin && (
                    <div>
                      <FormControlComponent
                        labelName="User Name"
                        fieldName="username"
                      />
                      {notify && error?.username ? (
                        <FormHelperText
                          error
                          className={classes.marginLeftText}
                        >{`User Name ${error.username[0]}`}</FormHelperText>
                      ) : undefined}
                    </div>
                  )}
                  <div>
                    <FormControlComponent labelName="Email" fieldName="email" />
                    {notify && error?.email ? (
                      <FormHelperText
                        error
                        className={classes.marginLeftText}
                      >{`Email ${error.email[0]}`}</FormHelperText>
                    ) : undefined}
                  </div>
                  <div>
                    <FormControlComponent
                      labelName="Password"
                      fieldName="password"
                      typeName={showPassword ? "text" : "password"}
                    />
                    {actionLogin && notify && error?.[0] ? (
                      <FormHelperText
                        error
                        className={classes.marginLeftText}
                      >{`Email or password ${error[0]}`}</FormHelperText>
                    ) : undefined}
                  </div>
                  {!actionLogin && (
                    <div>
                      <FormControlComponent
                        labelName="Confirm Password"
                        fieldName="confirmPassword"
                        typeName={showPassword ? "text" : "password"}
                      />
                    </div>
                  )}
                  <Checkbox
                    checked={showPassword}
                    color="primary"
                    onChange={handleShowPassword}
                  />
                  {` Check to display password`}
                  <br />
                  <Button
                    variant="contained"
                    color="primary"
                    size="medium"
                    type="submit"
                    disabled={!propsFormik.isValid}
                    className={classes.submitBtn}
                  >
                    Submit
                  </Button>
                  {actionLogin ? (
                    <div>
                      <p className={classes.text}>
                        You do not have an account ?
                        <Link to="/register"> Register now</Link>
                      </p>
                    </div>
                  ) : (
                    <div>
                      <p className={classes.text}>
                        You have an account?
                        <Link to="/login"> Sign in</Link>
                      </p>
                    </div>
                  )}
                </Form>
              );
            }}
          </Formik>
        </div>
      </div>
    </Grid>
  );
};

export default FormLog;
