import {
  Checkbox,
  Button,
  FormHelperText,
  makeStyles,
  createStyles,
} from "@material-ui/core";
import { Form, Formik, FormikProps, FormikValues } from "formik";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import * as Yup from "yup";
import { userAuthUpdate } from "../../model/userType";
import { RootState } from "../../stores/reducers";
import FormControlComponent from "../form log/FormControlComponent";
import PositionedSnackbar from "./Snackbar";

const useStyles = makeStyles(() =>
  createStyles({
    avatar: {
      display: "block",
      margin: "auto",
      maxWidth: "360px",
      marginBottom: "30px",
    },
    marginLeftText: {
      margin: "0px 8px",
    },
    marginBtn: {
      marginLeft: "8px",
      marginBottom: "32px",
    },
    heightDiv: {
      marginTop: "70px",
    },
  })
);

interface Props {
  onSubmitUser: (userUpdate: userAuthUpdate) => void;
}

const SettingSchema = Yup.object().shape({
  image: Yup.string()
    .url("URL is not valid")
    .matches(/\.(jpeg|jpg|gif|png)$/, "URL is not image URL"),
  username: Yup.string()
    .min(1, "User name too short!")
    .max(50, "User name too long!")
    .required("User name is required"),
  email: Yup.string().email("Invalid email").required("Email is required"),
  password: Yup.string()
    .min(8, "Password too short!")
    .max(50, "Password too long!"),
  confirmPassword: Yup.string().oneOf(
    [Yup.ref("password"), null],
    "Confirm password must equal password"
  ),
});

const FormSetting = ({ onSubmitUser }: Props) => {
  const [showPassword, setShowPassword] = useState(false);
  const [notify, setNotify] = useState(false);
  const [derror, setDerror] = useState(false);

  const currentUser = useSelector((state: RootState) => state.user);
  const error = useSelector((state: RootState) => state.user.error);

  const classes = useStyles();

  useEffect(() => {
    if (error) {
      setDerror(true);
    } else {
      setDerror(false);
    }
  }, [error]);

  const initialValue = {
    image: currentUser.image ? currentUser.image : "",
    username: currentUser.username,
    bio: currentUser.bio ? currentUser.bio : "",
    email: currentUser.email,
    password: "",
    confirmPassword: "",
  };

  const handleShowPassword = () => {
    setShowPassword(!showPassword);
  };

  return (
    <div className={classes.heightDiv}>
      <div className="container my-5">
        <h1 className="text-center mb-4">My Setting</h1>
        <Formik
          initialValues={initialValue}
          validationSchema={SettingSchema}
          onSubmit={(values) => {
            const userUpdate: userAuthUpdate = {
              username: values.username,
              email: values.email,
              password: values.password,
              image: values.image,
              bio: values.bio,
            };
            onSubmitUser(userUpdate);
          }}
        >
          {(propsFormik: FormikProps<FormikValues>) => {
            return (
              <Form className="row">
                <div className="mt-3 col-lg-6">
                  <img
                    src={propsFormik.values.image}
                    onError={(e) => {
                      e.currentTarget.src =
                        "https://png.pngtree.com/element_our/20200610/ourmid/pngtree-character-default-avatar-image_2237203.jpg";
                    }}
                    alt="avatar"
                    className={classes.avatar}
                  />
                </div>
                <div className="col-lg-6 d-flex justify-content-center">
                  <div>
                    <div>
                      <FormControlComponent
                        labelName="Image URL"
                        fieldName="image"
                      />
                    </div>
                    <div>
                      <FormControlComponent
                        labelName="User Name"
                        fieldName="username"
                      />
                      {derror && notify && error?.username ? (
                        <FormHelperText
                          error
                          className={classes.marginLeftText}
                        >{`User Name ${error.username[0]}`}</FormHelperText>
                      ) : undefined}
                    </div>
                    <div>
                      <FormControlComponent
                        labelName="Bio"
                        fieldName="bio"
                        row={8}
                      />
                    </div>
                    <div>
                      <FormControlComponent
                        labelName="Email"
                        fieldName="email"
                      />
                      {derror && notify && error?.email ? (
                        <FormHelperText
                          error
                          className={classes.marginLeftText}
                        >{`Email ${error.email[0]}`}</FormHelperText>
                      ) : undefined}
                    </div>
                    <div>
                      <div>
                        <FormControlComponent
                          labelName="Password"
                          fieldName="password"
                          typeName={showPassword ? "text" : "password"}
                        />
                      </div>
                    </div>
                    <div>
                      <div>
                        <FormControlComponent
                          labelName="Confirm Password"
                          fieldName="confirmPassword"
                          typeName={showPassword ? "text" : "password"}
                        />
                      </div>
                    </div>
                    <Checkbox
                      checked={showPassword}
                      color="primary"
                      onChange={handleShowPassword}
                    />
                    {` Check to display password`}
                    <br />
                    <Button
                      variant="contained"
                      color="primary"
                      size="medium"
                      type="submit"
                      disabled={!propsFormik.isValid}
                      className={classes.marginBtn}
                      onClick={() => {
                        setDerror(false);
                        setNotify(true);
                      }}
                    >
                      Submit
                    </Button>
                  </div>
                </div>
              </Form>
            );
          }}
        </Formik>
        <PositionedSnackbar
          error={error}
          notify={notify}
          success={currentUser.success}
        />
      </div>
    </div>
  );
};

export default FormSetting;
