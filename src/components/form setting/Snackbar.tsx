import React, { useEffect } from "react";
import Snackbar, { SnackbarOrigin } from "@material-ui/core/Snackbar";
import { Button, createStyles, makeStyles } from "@material-ui/core";

const useStyles = makeStyles(() =>
  createStyles({
    error: {
      color: "#fff",
      background: "#f44336",
    },
    success: {
      color: "#fff",
      background: "#4caf50",
    },
    warning: {
      color: "#fff",
      background: "#F18F01",
    },
  })
);

export interface State extends SnackbarOrigin {
  open: boolean;
}

interface Props {
  error?: any;
  notify: boolean;
  success?: any;
  warning?: any;
  onActionArticle?: (actionName: string) => void;
}

export default function PositionedSnackbar({
  error,
  notify,
  success,
  warning,
  onActionArticle,
}: Props) {
  const [state, setState] = React.useState<State>({
    open: false,
    vertical: "top",
    horizontal: "right",
  });
  const { vertical, horizontal, open } = state;

  const classes = useStyles();

  let messageNotify = undefined;
  let styleNotify = undefined;
  let statusSnackBar = "";
  if (error) {
    statusSnackBar = "error";
    messageNotify = "Update error";
    styleNotify = classes.error;
  } else if (success) {
    statusSnackBar = "success";
    messageNotify = "Update success";
    styleNotify = classes.success;
  } else if (warning) {
    statusSnackBar = "warning";
    messageNotify =
      "Article will be deleted in 5 seconds. Press the button to cancel";
    styleNotify = classes.warning;
  }

  const handleClose = (cancel?: string) => {
    if (onActionArticle && cancel) {
      onActionArticle("cancel");
    } else if (onActionArticle) {
      onActionArticle("delete");
    }
    setState({ ...state, open: false });
  };

  useEffect(() => {
    if (notify) {
      setState({
        open: true,
        vertical: "top",
        horizontal: "right",
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [error, success]);

  const handleAction = (
    <Button
      color="primary"
      size="small"
      variant="contained"
      onClick={() => {
        handleClose("cancel");
      }}
    >
      Cancel
    </Button>
  );

  return (
    <div>
      {statusSnackBar && (
        <Snackbar
          anchorOrigin={{ vertical, horizontal }}
          open={open}
          onClose={() => {
            handleClose();
          }}
          message={messageNotify}
          autoHideDuration={5000}
          key={vertical + horizontal}
          ContentProps={{
            className: styleNotify,
          }}
          action={warning ? handleAction : undefined}
        />
      )}
    </div>
  );
}
