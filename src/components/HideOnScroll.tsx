import React from "react";
import useScrollTrigger from "@material-ui/core/useScrollTrigger";
import Slide from "@material-ui/core/Slide";
import { motion } from "framer-motion";

interface Props {
  children: React.ReactElement;
}
const transition = { duration: 1.2, ease: [0.43, 0.23, 0.23, 0.96] };

export default function HideOnScroll(props: Props) {
  const { children } = props;
  const trigger = useScrollTrigger();
  return (
    <motion.div initial={{ y: -80 }} animate={{ y: 0 }} transition={transition}>
      <Slide appear={false} direction="down" in={!trigger}>
        {children}
      </Slide>
    </motion.div>
  );
}
