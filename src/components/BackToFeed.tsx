import React from "react";
import Toolbar from "@material-ui/core/Toolbar";
import useScrollTrigger from "@material-ui/core/useScrollTrigger";
import Box from "@material-ui/core/Box";
import Container from "@material-ui/core/Container";
import Zoom from "@material-ui/core/Zoom";

interface Props {
  children: React.ReactElement;
}

// scroll top function

export function ScrollTop(props: Props) {
  const { children } = props;
  //   const classes = useStyles();

  const trigger = useScrollTrigger({
    disableHysteresis: true,
    threshold: 100,
  });

  const handleClick = (event: React.MouseEvent<HTMLDivElement>) => {
    const anchor = (
      (event.target as HTMLDivElement).ownerDocument || document
    ).querySelector("#back-to-feed-anchor");

    if (anchor) {
      anchor.scrollIntoView({ behavior: "smooth", block: "start" });
    }
  };

  return (
    <Zoom in={trigger}>
      <div onClick={handleClick} role="presentation">
        {children}
      </div>
    </Zoom>
  );
}

// back to top component

interface BackFeedProps {
  children: React.ReactElement;
  handleProfile?: (user: string) => void;
}

export default function BackToFeed(props: BackFeedProps) {
  return (
    <React.Fragment>
      <Toolbar id="back-to-feed-anchor"></Toolbar>
      <Container>
        <Box my={2}>{props.children}</Box>
      </Container>
    </React.Fragment>
  );
}
