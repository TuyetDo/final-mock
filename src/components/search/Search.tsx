import {
  createStyles,
  FormControl,
  FormHelperText,
  makeStyles,
  TextField,
  Theme,
} from "@material-ui/core";
import { Field, FieldProps } from "formik";
import React from "react";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: "flex",
      flexWrap: "wrap",
    },
    textField: {
      marginLeft: theme.spacing(1),
      marginRight: theme.spacing(1),
      width: "40ch",
    },
  })
);

interface Props {
  handleSearch: (term: string) => void;
}

const Search = ({ handleSearch }: Props) => {
  const classes = useStyles();
  return (
    <Field name="search">
      {(propsField: FieldProps) => {
        const { field, meta } = propsField;
        return (
          <FormControl className={classes.root} error>
            <TextField
              {...field}
              label="search"
              placeholder="Search for your favorite author"
              margin="normal"
              variant="outlined"
              className={classes.textField}
              type="text"
            />
            {meta.touched && meta.error && (
              <FormHelperText className={classes.textField}>
                {meta.error}
              </FormHelperText>
            )}
          </FormControl>
        );
      }}
    </Field>
  );
};

export default Search;
