import React, { useEffect } from "react";
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import TrendingArticleItem from "../article-item/TrendingArticleItem";
import PopularTags from "../popular-tag/PopularTags";
import { useDispatch, useSelector } from "react-redux";
import { getArticleTrending } from "../../stores/action/actionCreators/ArticleTrendingAction";
import { RootState } from "../../stores/reducers";
import { Article } from "../../model/articleType";
import BookOfMonthItem from "../article-item/BookOfMonthITem";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
    },
    paper: {
      marginBottom: "25px",
      padding: theme.spacing(2),
      textAlign: "center",
      color: theme.palette.text.secondary,
    },
  })
);

interface Prop {
  handleProfile: (user: string) => void;
}

const Trending = ({ handleProfile }: Prop) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getArticleTrending("HuManIty"));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const articlesTrending = useSelector(
    (state: RootState) => state.trending.articles
  );
  let favoritesCount: any;
  let ListOfTrend;
  if (articlesTrending) {
    favoritesCount = articlesTrending.map((article: Article) => {
      return article.favoritesCount;
    });

    const sortedCount = favoritesCount.sort((a: number, b: number) => a - b);

    const number1Trend = articlesTrending.filter((article: Article) => {
      return article.favoritesCount === sortedCount[sortedCount.length - 1];
    });
    const number2Trend = articlesTrending.filter((article: Article) => {
      return article.favoritesCount === sortedCount[sortedCount.length - 2];
    });
    const number3Trend = articlesTrending.filter((article: Article) => {
      return article.favoritesCount === sortedCount[sortedCount.length - 3];
    });

    ListOfTrend = [...number1Trend, ...number2Trend, ...number3Trend];
  }

  const handleClick = (tag: string) => {
    dispatch(getArticleTrending(tag));
  };
  return (
    <div className="my-4">
      <h6>TRENDING</h6>
      <div className={classes.root}>
        <Grid container spacing={3}>
          <Grid item xs={12} sm={6} lg={8}>
            <Paper className={classes.paper}>
              <TrendingArticleItem
                trendingArticle={ListOfTrend}
                handleProfile={handleProfile}
              />
            </Paper>
          </Grid>
          <Grid item xs={12} sm={6} lg={4}>
            <Paper className={classes.paper}>
              <PopularTags handleClick={handleClick} />
            </Paper>
            <Paper className={classes.paper}>
              <h6 className="text-uppercase">Your ad here</h6>
              <BookOfMonthItem />
            </Paper>
          </Grid>
        </Grid>
      </div>
    </div>
  );
};

export default Trending;
