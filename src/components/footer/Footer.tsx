import React from "react";
import styled from "styled-components";

const StyledFooter = styled.div`
  height: 85px;
  background-color: rgb(4, 28, 45);
  /* position: fixed; */
  bottom: 0px;
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  color: white;
`;

const Footer = () => {
  return (
    <StyledFooter>
      <p>Copyright © Respective Authors. All rights reserved.</p>
    </StyledFooter>
  );
};

export default Footer;
