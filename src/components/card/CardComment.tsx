import React, { useState } from "react";

import {
  makeStyles,
  Theme,
  createStyles,
  Card,
  CardHeader,
  Avatar,
  IconButton,
  CardContent,
  Typography,
  CardActions,
} from "@material-ui/core";
import FavoriteIcon from "@material-ui/icons/Favorite";
import DeleteIcon from "@material-ui/icons/Delete";
import ShareIcon from "@material-ui/icons/Share";
import { red } from "@material-ui/core/colors";
import { Comments } from "../../model/articleType";
import { convertDate } from "../../configs/date/getDate";
import { Link } from "react-router-dom";
import { RootState } from "../../stores/reducers";
import { useSelector } from "react-redux";

interface Props {
  item: Comments;
  onRemove: (item: number) => void;
}
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: "100%",
    },
    media: {
      height: 0,
      paddingTop: "56.25%", // 16:9
    },
    expand: {
      transform: "rotate(0deg)",
      marginLeft: "auto",
      transition: theme.transitions.create("transform", {
        duration: theme.transitions.duration.shortest,
      }),
    },
    expandOpen: {
      transform: "rotate(180deg)",
    },
    avatar: {
      backgroundColor: red[500],
    },
    cardAction: {
      justifyContent: "space-around",
    },
    likeBtn: {
      display: "block",
      margin: "auto",
    },
  })
);

export default function RecipeReviewCard({ item, onRemove }: Props) {
  const classes = useStyles();
  const [liked, setLiked] = useState(false);
  const [shared, setShared] = useState(false);
  const currentUser = useSelector((state: RootState) => state.user);

  return (
    <Card className={classes.root}>
      <CardHeader
        avatar={
          <Avatar
            aria-label="recipe"
            className={classes.avatar}
            src={item.author ? item.author.image : ""}
          ></Avatar>
        }
        title={<Link to={`/`}>{item.author ? item.author.username : ""}</Link>}
        subheader={item ? convertDate(item.updatedAt) : ""}
      />
      <CardContent>
        <Typography variant="body2" color="textSecondary" component="p">
          {item ? item.body : ""}
        </Typography>
      </CardContent>
      <CardActions disableSpacing className={classes.cardAction}>
        <IconButton
          aria-label="add to favorites"
          onClick={() => setLiked(!liked)}
        >
          {liked ? <FavoriteIcon color="primary" /> : <FavoriteIcon />}
        </IconButton>
        {item.author.username === currentUser.username ? (
          <IconButton aria-label="remove" onClick={() => onRemove(item.id)}>
            <DeleteIcon />
          </IconButton>
        ) : undefined}
        <IconButton
          aria-label="Share article"
          onClick={() => setShared(!shared)}
        >
          {shared ? <ShareIcon color="primary" /> : <ShareIcon />}
        </IconButton>
      </CardActions>
    </Card>
  );
}
