import React from "react";

const Banner = () => {
  return (
    <header className="text-uppercase text-center my-5">
      <h1>The book lover's blog</h1>
      <i>A BLOG FOR PEOPLE WHO LOVE BOOKS AS MUCH AS ME</i>
    </header>
  );
};

export default Banner;
