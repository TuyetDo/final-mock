import { Grid } from "@material-ui/core";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useLocation } from "react-router-dom";
import FormLog from "../../components/form log/FormLog";
import Header from "../../components/header/Header";
import { userInputInfo } from "../../model/userType";
import {
  login,
  register,
} from "../../stores/action/actionCreators/AuthActions";
import { RootState } from "../../stores/reducers";

const LogPage = () => {
  let { pathname } = useLocation();
  const history = useHistory();
  const dispatch = useDispatch();
  const isLogin = useSelector((state: RootState) => state.user.isLogin);
  pathname = pathname.split("/")[1];

  let actionLogin = false;
  if (pathname === "login") {
    actionLogin = true;
  }

  const checkLogin = isLogin || localStorage.getItem("token");

  const handleSubmitUser = (newUser: userInputInfo) => {
    let action;
    if (actionLogin) {
      action = login(newUser);
    } else {
      action = register(newUser);
    }
    dispatch(action);
  };

  useEffect(() => {
    if (checkLogin) {
      history.push("/");
    }
  }, [checkLogin, history]);

  return (
    <Grid xs={12}>
      <Header />
      {!checkLogin && (
        <FormLog onSubmitUser={handleSubmitUser} actionLogin={actionLogin} />
      )}
    </Grid>
  );
};

export default LogPage;
