import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import ArticlesList from "../../components/articles-list/ArticlesList";
import Banner from "../../components/banner/Banner";
import PaginationFeature from "../../components/pagination/PaginationFeature";
import Trending from "../../components/trending/Trending";
import { getArticles } from "../../stores/action/actionCreators/ArticleActions";
import { getPageArticles } from "../../stores/action/actionCreators/PaginationAction";
import { RootState } from "../../stores/reducers";
import BackToTop from "../../components/BackTop";
import { motion } from "framer-motion";
import { getProfile } from "../../stores/action/actionCreators/ProfileAction";
import Footer from "../../components/footer/Footer";
import YourFeed from "../../components/feed/YourFeed";
import { getArticleFeed } from "../../stores/action/actionCreators/FeedActions";
import {
  createStyles,
  makeStyles,
  Paper,
  Tab,
  Tabs,
  Theme,
  Typography,
} from "@material-ui/core";
import BackToFeed from "../../components/BackToFeed";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
    },
  })
);

const Home = () => {
  const dispatch = useDispatch();
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const isLogin = useSelector((state: RootState) => state.user.isLogin);
  const feeds = useSelector((state: RootState) => state.feeds.articles);

  const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    setValue(newValue);
  };

  useEffect(() => {
    dispatch(getArticles());
    dispatch(getArticleFeed());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handlePage = (page: number) => {
    const offset = (page - 1) * 20;
    dispatch(getPageArticles(offset));
  };
  const handleProfile = (user: string) => {
    // if (user.toLowerCase().indexOf("@") > 0) {
    //   dispatch(getProfile(user.toLowerCase().slice(0, user.indexOf("@"))));
    // } else {
    //   dispatch(getProfile(user.toLowerCase()));
    // }
    dispatch(getProfile(user));
  };

  const articles = useSelector((state: RootState) => state.articles.articles);

  const transition = { duration: 1.2, ease: [0.43, 0.23, 0.23, 0.96] };

  const articlesListProp = {
    articles,
    handleProfile,
  };
  return (
    <motion.div
      exit={{ opacity: 0 }}
      transition={transition}
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
    >
      <BackToTop>
        <div className="container" style={{ marginTop: "130px" }}>
          <Banner />
          <Trending handleProfile={handleProfile} />
          <BackToFeed>
            <Paper className={classes.root}>
              <Tabs
                value={value}
                onChange={handleChange}
                indicatorColor="primary"
                // centered
              >
                <Tab label={isLogin ? "GLOBAL" : "NEW POST"} />
                {isLogin ? <Tab label="YOUR FEED" /> : <Tab disabled />}
              </Tabs>
            </Paper>
          </BackToFeed>
          {value === 0 ? (
            <ArticlesList {...articlesListProp} handleProfile={handleProfile} />
          ) : (
            <YourFeed articles={feeds} handleProfile={handleProfile} />
          )}

          {value === 0 ? (
            <PaginationFeature handlePage={handlePage} />
          ) : (
            <div>
              {feeds.length < 21 ? (
                <Typography style={{ height: "50px" }} color="primary">
                  You created total {feeds.length} articles.
                </Typography>
              ) : (
                <PaginationFeature handlePage={handlePage} />
              )}
            </div>
          )}
        </div>
      </BackToTop>
      <Footer />
    </motion.div>
  );
};

export default Home;
