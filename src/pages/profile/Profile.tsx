import {
  Paper,
  Tabs,
  Tab,
  createStyles,
  makeStyles,
  Theme,
  Typography,
} from "@material-ui/core";
import React, { useState } from "react";
import { useEffect } from "react";

import { useDispatch, useSelector } from "react-redux";
import { useLocation } from "react-router-dom";

import BackToFeed from "../../components/BackToFeed";
import BackToTop from "../../components/BackTop";

import PositionedSnackbar from "../../components/form setting/Snackbar";
import {
  cancelDeleteArticle,
  deleteArticleNow,
} from "../../stores/action/actionCreators/ArticleActions";

import YourFeed from "../../components/feed/YourFeed";
import PaginationFeature from "../../components/pagination/PaginationFeature";
import ShowProfileArticle from "../../components/profile-user/ShowProfileArticle";
import { GetArticles } from "../../configs/api/handleCallApi";
import { Article } from "../../model/articleType";

import { getPageArticles } from "../../stores/action/actionCreators/PaginationAction";
import { getProfile } from "../../stores/action/actionCreators/ProfileAction";

import { RootState } from "../../stores/reducers";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
    },
  })
);

interface TabPanelProps {
  children?: React.ReactNode;
  index: any;
  value: any;
}

function TabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && <div>{children}</div>}
    </div>
  );
}

function a11yProps(index: any) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

const Profile = () => {
  const dispatch = useDispatch();
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  const profile = useSelector((state: RootState) => state.profile);

  const user = useSelector((state: RootState) => state.user);
  const notification = useSelector(
    (state: RootState) => state.notification.notify
  );
  const [offset, setOffset] = useState(0);

  const location = useLocation();
  const author = location.pathname.slice(
    location.pathname.lastIndexOf("/") + 1
  );
  const [listArticle, setListArticle] = useState<Article[]>([]);

  const handleActionArticle = (actionName: string) => {
    if (actionName === "delete") {
      const action = deleteArticleNow();
      dispatch(action);
      if (listArticle.length > 0) {
        let articleAfter = [...listArticle];
        articleAfter.shift();
        setListArticle(articleAfter);
      }
    } else {
      const action = cancelDeleteArticle();
      dispatch(action);
    }
  };

  const [listFavorite, setListFavorite] = useState<Article[]>([]);
  useEffect(() => {
    dispatch(getProfile(author));

    const data = GetArticles(undefined, author, undefined, 6, offset);
    data
      .then((response) => {
        setListArticle(response.articles);
      })
      .catch((error) => {
        console.log(error.message);
      });

    const lastProfile = profile[profile.length - 1];

    const favorites = GetArticles(undefined, undefined, author, 6, offset);
    favorites
      .then((response) => {
        setListFavorite(response.articles);
      })
      .catch((error) => {
        console.log(error.message);
      });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dispatch, author, offset]);

  const lastProfile = profile && profile[profile.length - 1];

  const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    setValue(newValue);
  };

  const handlePage = (page: number) => {
    const offsetValue = (page - 1) * 20;
    setOffset(offsetValue);
    dispatch(getPageArticles(offsetValue));
  };

  const handleProfile = (user: string) => {
    dispatch(getProfile(user));
  };

  // const articlesListProp = {
  //   articles,
  //   handleProfile,
  // };

  return (
    <>
      <BackToTop>
        <div className="container" style={{ marginTop: "130px" }}>
          {/* <Banner />
          <Trending handleProfile={handleProfile} /> */}
          <ShowProfileArticle
            lastProfile={lastProfile && lastProfile}
            user={user && user}
          />
          <BackToFeed>
            <Paper className={classes.root}>
              <Tabs
                value={value}
                onChange={handleChange}
                indicatorColor="primary"
                // centered
              >
                <Tab label="My Article" {...a11yProps(0)} />
                <Tab label="Favorited Article" {...a11yProps(1)} />
              </Tabs>

              <TabPanel value={value} index={0}>
                <div>
                  <YourFeed
                    articles={listArticle && listArticle}
                    handleProfile={handleProfile}
                  />

                  {listArticle && listArticle.length < 6 ? (
                    <Typography style={{ height: "50px" }} color="primary">
                      You created total {listArticle && listArticle.length}{" "}
                      articles.
                    </Typography>
                  ) : (
                    <PaginationFeature
                      handlePage={handlePage}
                      total={listArticle && listArticle.length}
                    />
                  )}
                </div>
              </TabPanel>
              <TabPanel value={value} index={1}>
                <YourFeed
                  articles={listFavorite && listFavorite}
                  handleProfile={handleProfile}
                />
                {listFavorite && listFavorite.length < 6 ? (
                  <Typography style={{ height: "50px" }} color="primary">
                    You created total {listFavorite && listFavorite.length}{" "}
                    articles.
                  </Typography>
                ) : (
                  <PaginationFeature
                    handlePage={handlePage}
                    total={listFavorite && listFavorite.length}
                  />
                )}
              </TabPanel>
            </Paper>
          </BackToFeed>
          {/* {value === 0 ? (
            <ArticlesList {...articlesListProp} handleProfile={handleProfile} />
          ) : (
            <YourFeed articles={feeds} handleProfile={handleProfile} />
          )} */}
          {/* {value === 0 ? (
            <YourFeed articles={feeds} handleProfile={handleProfile} />
          ) : null} */}

          {/* {value === 0 ? (
            <PaginationFeature handlePage={handlePage} />
          ) : (
            <div>
              {feeds && feeds.length < 21 ? (
                <Typography style={{ height: "50px" }} color="primary">
                  You created total {feeds.length} articles.
                </Typography>
              ) : (
                <PaginationFeature handlePage={handlePage} />
              )}
            </div>

          ) : (
            "Not found infomation of this user"
          )}
          

          )} */}
          <PositionedSnackbar
            notify={notification}
            warning="delete"
            onActionArticle={handleActionArticle}
          />
        </div>
      </BackToTop>
    </>
  );
};

export default Profile;
