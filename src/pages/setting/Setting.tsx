import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import BackToTop from "../../components/BackTop";
import FormSetting from "../../components/form setting/FormSetting";
import { userAuthUpdate } from "../../model/userType";
import { updateUser } from "../../stores/action/actionCreators/AuthActions";
import { RootState } from "../../stores/reducers";

const Setting = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const checkUser = useSelector((state: RootState) => state.user.username);
  const checkLogin = localStorage.getItem("token");

  const handleSubmitUser = (userUpdate: userAuthUpdate) => {
    const action = updateUser(userUpdate);
    dispatch(action);
  };

  useEffect(() => {
    if (!checkLogin) {
      history.push("/");
    }
  }, [checkLogin, history]);

  return (
    <BackToTop>
      <div>{checkUser && <FormSetting onSubmitUser={handleSubmitUser} />}</div>
    </BackToTop>
  );
};

export default Setting;
