import React, { useEffect } from "react";
import {
  Button,
  createStyles,
  Grid,
  makeStyles,
  Paper,
  Theme,
} from "@material-ui/core";
import { ShowDetailArticle } from "../../components/detail article/ShowDetailArticle";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { Article } from "../../model/articleType";
import { RootState } from "../../stores/reducers";
import { Container } from "@material-ui/core";
import { useState } from "react";
import { CommentArticle } from "../../components/comment/CommentArticle";

import { useHistory, useParams } from "react-router-dom";
import { getComments } from "../../stores/action/actionCreators/CommentActions";
import {
  deleteArticle,
  getArticleSlug,
} from "../../stores/action/actionCreators/ArticleActions";

import BackToTop from "../../components/BackTop";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    // root: {
    //   padding: theme.spacing(2),
    //   // textAlign: "center",
    //   // color: theme.palette.text.secondary,
    //   display: "flex",
    //   position: "relative",
    // },
    paper: {
      padding: theme.spacing(2),
      textAlign: "center",
      color: theme.palette.text.secondary,
    },

    header: {
      display: "flex",
      flexDirection: "column",
      justifyContent: "center",
      alignItems: "center",
      marginTop: "20px",
    },
    editBtn: {
      marginRight: "5px",
    },
  })
);
export const DetailArticle = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const history = useHistory();
  let { slug } = useParams<{ slug: string }>();
  const [open, setOpen] = useState(false);
  const currentUser = useSelector((state: RootState) => state.user);

  const article: Article = useSelector((state: RootState) => {
    return state.article;
  }, shallowEqual);

  const author = article.author;
  const favorite = useSelector((state: RootState) => state.favorited);

  const [follow, setFollow] = useState<boolean>(author && author.following);

  useEffect(() => {
    dispatch(getArticleSlug(slug));
    dispatch(getComments(slug));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleComment = (value?: boolean) => {
    if (value !== undefined) {
      return setOpen(value);
    }
    return setOpen(!open);
  };

  const showCountFavorite = () => {
    if (article && favorite && favorite.article) {
      return favorite.article.favoritesCount;
    } else if (article) {
      return article.favoritesCount;
    } else if (favorite && favorite.article) {
      return favorite.article.favoritesCount;
    }
  };

  const handleClickEdit = () => {
    history.push(`/editor/${slug}`);
  };

  const handleClickDelete = () => {
    const action = deleteArticle(slug);
    dispatch(action);
    history.push(`/profile/${author.username}`);
  };

  return (
    <div>
      <BackToTop>
        {/* <h1>Hello</h1> */}
        <div>
          <Container maxWidth="md">
            <div className={classes.header}>
              {article && author ? (
                <>
                  <h1>{article.title}</h1>
                  <Grid
                    container
                    spacing={3}
                    className="d-flex justify-content-center"
                  >
                    <Grid item xs={12} sm={8}>
                      <Paper className={classes.paper}>
                        <ShowDetailArticle
                          img={author.image}
                          username={author.username}
                          update={article.updatedAt}
                          following={follow}
                          favorited={article.favorited}
                          favoritesCount={article.favoritesCount}
                          // onFollow={handleFollowing}
                          body={article.body}
                          tags={article.tagList}
                          title={article.title}
                          showCountFavorite={showCountFavorite}
                          handleComment={handleComment}
                          open={open}
                        />
                      </Paper>
                    </Grid>
                  </Grid>
                </>
              ) : null}
            </div>
          </Container>
          {article && author && open ? (
            <CommentArticle
              parameter={slug}
              openProp={open}
              onChangeOpen={handleComment}
              username={author.username}
              img={author.image}
            />
          ) : null}
          {currentUser?.username === author?.username && (
            <div className="d-flex justify-content-center">
              <Button
                color="primary"
                variant="contained"
                onClick={handleClickEdit}
                className={classes.editBtn}
              >
                Edit article
              </Button>
              <Button
                color="secondary"
                variant="contained"
                onClick={handleClickDelete}
              >
                Delete article
              </Button>
            </div>
          )}
        </div>
      </BackToTop>
    </div>
  );
};
