import React, { useEffect, useState } from "react";
import { Field, FieldProps, Form, Formik } from "formik";
import * as yup from "yup";
import {
  ActionNameNotify,
  NewArticleInterface,
} from "../../model/newArticleType";
import {
  Button,
  Chip,
  Container,
  createStyles,
  makeStyles,
} from "@material-ui/core";
import FormControlNewArticle from "../../components/form new article/FormControlNewArticle";
import {
  CreatNewArticle,
  EditArticleSlug,
} from "../../configs/api/handleCallApi";
import { useHistory } from "react-router-dom";
import FormControlComponent from "../../components/form log/FormControlComponent";
import BackToTop from "../../components/BackTop";
import { useSelector } from "react-redux";
import { RootState } from "../../stores/reducers";

const useStyles = makeStyles(() =>
  createStyles({
    submitBtn: {
      margin: "0px 7px",
    },
    styleDiv: {
      marginTop: "70px",
      marginBottom: "30px",
    },
    styleH1: {
      textAlign: "center",
    },
  })
);

interface Props {
  initialValuesProps?: NewArticleInterface;
  slug?: string;
  onNotify?: () => void;
  actionName?: ActionNameNotify;
}

const validateSchema = yup.object().shape({
  title: yup.string().required("Title is required").min(3),
  description: yup.string().required("Description is required").min(3),
  body: yup.string().required("About article is required").min(3),
});

export const EditorComponent = ({ initialValuesProps, slug }: Props) => {
  const history = useHistory();
  const [tagArray, setTagArray] = useState<string[]>([]);
  const currentUser = useSelector((state: RootState) => state.user);

  const classes = useStyles();

  let initialValue: NewArticleInterface | undefined;
  if (slug) {
    initialValue = initialValuesProps;
  } else {
    initialValue = {
      title: "",
      description: "",
      body: "",
      tagList: [],
    };
  }

  useEffect(() => {
    if (initialValuesProps) {
      setTagArray([...initialValuesProps.tagList]);
    }
  }, [initialValuesProps]);

  const handleAddTag = (newTagArray: string[]) => {
    setTagArray([...newTagArray]);
  };

  const handleRemoveTag = (tagItem: string) => {
    const index = tagArray.indexOf(tagItem);
    const newTagArray = tagArray
      .slice(0, index)
      .concat(tagArray.slice(index + 1));
    setTagArray([...newTagArray]);
  };

  const handleSumbit = (value: NewArticleInterface) => {
    const newArticle = {
      ...value,
      tagList: tagArray,
    };
    if (slug) {
      EditArticleSlug({
        article: newArticle,
        slug,
      });
    } else {
      CreatNewArticle({
        article: newArticle,
      });
    }
    history.push(`/profile/${currentUser?.username}`);
    window.location.reload();
  };

  return (
    <BackToTop>
      <Container className="container mt-5">
        <div className={classes.styleDiv}>
          <h1 className={classes.styleH1}>
            {!slug ? "Create Article" : "Edit Article"}
          </h1>
          <div className="row d-flex justify-content-center">
            {initialValue && (
              <Formik
                initialValues={initialValue}
                validationSchema={validateSchema}
                onSubmit={handleSumbit}
              >
                {(propsFormik) => (
                  <Form>
                    <FormControlComponent labelName="Title" fieldName="title" />
                    <FormControlComponent
                      labelName="Description Article"
                      fieldName="description"
                      placeholder="What's this article about"
                    />
                    <FormControlComponent
                      labelName="About Article"
                      fieldName="body"
                      placeholder="Write your article (in markdown)"
                      row={10}
                    />
                    <Field name="tagList">
                      {(propsField: FieldProps) => (
                        <FormControlNewArticle
                          propsField={propsField}
                          labelName="Tag"
                          placeholder="Enter tags"
                          onAddTag={handleAddTag}
                          oldTagArray={initialValuesProps?.tagList}
                          newTagArray={tagArray}
                        />
                      )}
                    </Field>
                    {tagArray.map((tagItem) => {
                      return (
                        <small key={tagItem}>
                          <Chip
                            label={tagItem}
                            onDelete={() => handleRemoveTag(tagItem)}
                            color="primary"
                            variant="outlined"
                            className="my-2 mx-1"
                          />
                        </small>
                      );
                    })}
                    <br />
                    <Button
                      variant="contained"
                      color="primary"
                      size="medium"
                      type="submit"
                      disabled={!propsFormik.isValid}
                      className={classes.submitBtn}
                    >
                      Publish Article
                    </Button>
                  </Form>
                )}
              </Formik>
            )}
          </div>
        </div>
      </Container>
    </BackToTop>
  );
};
