import React from "react";
import { EditorComponent } from "./EditorComponent";

const NewArticle = () => {
  return (
    <div>
      <EditorComponent />
    </div>
  );
};

export default NewArticle;
