import React, { useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import { GetArticleSlug } from "../../configs/api/handleCallApi";
import { NewArticleInterface } from "../../model/newArticleType";
import { EditorComponent } from "./EditorComponent";

const EditArticle = () => {
  const [article, setArticle] = useState<NewArticleInterface>();
  let { pathname } = useLocation();
  const slug = pathname.split("/")[2];

  useEffect(() => {
    (async () => {
      try {
        const data = await GetArticleSlug(slug);
        const oldArticle = {
          title: data.article.title,
          body: data.article.body,
          description: data.article.description,
          tagList: data.article.tagList,
        };
        setArticle(oldArticle);
      } catch (error) {
        console.log(error);
      }
    })();
  }, [slug]);
  return (
    <div>
      <EditorComponent initialValuesProps={article} slug={slug} />
    </div>
  );
};

export default EditArticle;
