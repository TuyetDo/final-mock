import book1 from './book1.jpeg'
import book2 from './book2.jpeg'
import book3 from './book3.jpeg'
import book4 from './book4.jpeg'
import book5 from './book5.jpeg'
import book6 from './book6.jpeg'
import book7 from './book7.jpeg'
import book8 from './book8.jpeg'
import book9 from './book9.jpeg'
import book10 from './book10.gif'
import book11 from './book11.jpeg'
import book12 from './book12.jpeg'
import book13 from './book13.jpeg'
import book14 from './book14.jpeg'
import book15 from './book15.jpeg'
import book16 from './book16.jpeg'
import book17 from './book17.jpeg'
import book18 from './book18.jpeg'
import book19 from './book19.jpeg'
import book20 from './book20.gif'

export const images = [
    book1, book2, book3, book4, book5, book6, book7, book8, book9, book10, book11, book12, book13, book14, book15, book16, book17, book18, book19, book20
]