import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getUser } from "../stores/action/actionCreators/AuthActions";
import { RootState } from "../stores/reducers";
// import { Auth } from "./Auth";
import Homes from "./Home";

const Views = () => {
  const isLogin = useSelector((state: RootState) => state.user.isLogin);
  // const username = useSelector((state: RootState) => state.user.username);

  const dispatch = useDispatch();
  const token = localStorage.getItem("token");

  // test check user have token and load get user to display header
  useEffect(() => {
    if (!isLogin && token) {
      const action = getUser();
      dispatch(action);
    }
  }, [isLogin, token, dispatch]);

  return (
    <div>
      <Homes />
      {/* {username ? <Homes /> : <p>Loading</p>} */}
    </div>
  );
};

export default Views;
