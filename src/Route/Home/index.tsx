import React from "react";
import { IRouter } from "../../model/routerType";
import { routers } from "../../configs/router/router";
import { Route, Switch, BrowserRouter } from "react-router-dom";
import { AnimatePresence } from "framer-motion";

const Homes = () => {
  const showContent = (routes: IRouter[]) => {
    var result = null;
    if (routes.length > 0) {
      result = routes.map((route, index) => {
        return (
          <Route
            key={index}
            path={route.path}
            exact={route.exact}
            component={route.main}
          />
        );
      });
    }
    return <Switch>{result}</Switch>;
  };
  // initial={false}
  return (
    <div>
      <AnimatePresence exitBeforeEnter>
        <BrowserRouter>{showContent(routers)}</BrowserRouter>
      </AnimatePresence>
    </div>
  );
};

export default Homes;
