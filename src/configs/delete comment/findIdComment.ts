import { Comments } from "../../model/articleType";

export const findIdComment = (value: Comments[], responseCommentId: number) => {
  let id = 0;
  for(let i = 0; i < value.length; i++){
    if(value[i].id === responseCommentId){
      id = i
      return id
    }
  }
  return id
}
