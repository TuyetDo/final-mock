import React, { Fragment } from "react";
import { IRouter } from "../../model/routerType";
import { DetailArticle } from "../../pages/detail article/DetailArticle";
import Home from "../../pages/home/Home";
import LogPage from "../../pages/log page/LogPage";
import EditArticle from "../../pages/new article/EditArticle";
import NewArticle from "../../pages/new article/NewArticle";
import Profile from "../../pages/profile/Profile";

import Setting from "../../pages/setting/Setting";

const withMenu = (component: JSX.Element) => <Fragment>{component}</Fragment>;

export const routers: IRouter[] = [
  {
    path: "/",
    exact: true,
    redirect: "/",
    main: () => withMenu(<Home />),
  },
  {
    path: "/editor",
    main: () => withMenu(<NewArticle />),
    exact: true,
  },
  {
    path: "/login",
    main: () => withMenu(<LogPage />),
    exact: true,
  },
  {
    path: "/register",
    main: () => withMenu(<LogPage />),
    exact: true,
  },
  {
    path: "/setting",
    main: () => withMenu(<Setting />),
  },
  {
    path: "/articles/:slug",
    main: () => withMenu(<DetailArticle />),
    exact: false,
  },
  {
    path: "/profile/:author",
    main: () => withMenu(<Profile />),
    exact: false,
  },
  {
    path: "/editor/:slug",
    main: () => withMenu(<EditArticle />),
    exact: true,
  },
];
