import axios from "axios";
import { BASEURL } from "../../contant";
import {
  CommentCreate,
  PayloadArticleFavorite,
  PayloadArticleFollow,
  payloadCommentDelete,
} from "../../model/articleType";
import { NewArticleInterface } from "../../model/newArticleType";

// create instance

const axiosService = axios.create({
  baseURL: BASEURL,
  headers: {
    "Content-Type": "application/json",
  },
});

// set token for header

const setAuthToken = () => {
  return axios.create({
    baseURL: BASEURL,
    headers: {
      Authorization: `Token ${localStorage.getItem("token")}`,
      "Content-Type": "application/json",
    },
  });
};

//handle user login, register

export const Login = async (payload: {
  user: { email: string; password: string };
}) => {
  try {
    const response = await axiosService.post("/users/login", payload);
    if (response.status === 200) {
      localStorage.setItem("token", response.data.user.token);
    }
    return response;
  } catch (error) {
    return error.response;
  }
};

export const Register = async (payload: {
  user: { username: string; email: string; password: string };
}) => {
  try {
    const response = await axiosService.post("/users", payload);
    if (response.status === 200) {
      localStorage.setItem("token", response.data.user.token);
    }
    return response;
  } catch (error) {
    return error.response;
  }
};

export const GetUser = async () => {
  try {
    const axiosServiceAuth = setAuthToken();
    const response = await axiosServiceAuth.get("/user");
    return response.data;
  } catch (error) {
    return error.response;
  }
};

// handle get article

export const GetArticles = async (
  tag?: string,
  author?: string,
  favorited?: string,
  limit: number = 20,
  offset: number = 0
) => {
  try {
    const response = await axiosService.get("/articles", {
      params: {
        tag: tag,
        author: author,
        favorited: favorited,
        limit: limit,
        offset: offset,
      },
    });
    return response.data;
  } catch (error) {
    return error.response;
  }
};

// Get Feed Articles

export const GetFeed = async () => {
  try {
    const axiosServiceAuth = setAuthToken();
    const response = await axiosServiceAuth.get("/articles/feed");
    return response.data;
  } catch (error) {
    return error.response;
  }
};

// handle get popular tags

export const GetPopularTags = async () => {
  try {
    const response = await axiosService.get("/tags");
    return response.data.tags;
  } catch (error) {
    return error.response;
  }
};

// update user
export const UpdateUser = async (payload: {
  user: {
    username: string;
    email: string;
    password?: string;
    image?: string;
    bio?: string;
  };
}) => {
  try {
    const axiosServiceAuth = setAuthToken();
    const response = await axiosServiceAuth.put("/user", payload);
    return response;
  } catch (error) {
    return error.response;
  }
};

//create new article

export const CreatNewArticle = async (payload: {
  article: NewArticleInterface;
}) => {
  try {
    const axiosServiceAuth = setAuthToken();
    const response = await axiosServiceAuth.post("/articles", payload);
    return response.data;
  } catch (error) {
    return error.message;
  }
};
// get item: slug article
export const getSlugArticle = async (slug: string) => {
  try {
    const response = await axiosService.get(`/articles/${slug}`);
    return response.data.article;
  } catch (error) {
    return error.response;
  }
};

//get profile

export const GetProfile = async (user: string) => {
  try {
    const response = await axiosService.get(`/profiles/${user}`);
    return response.data;
  } catch (error) {
    return error.response;
  }
};

export const getCommentsApi = async (commentId: {
  type: string;
  payload: string;
}) => {
  try {
    const response = await axiosService.get(
      `/articles/${commentId.payload}/comments`
    );
    return response.data.comments;
  } catch (error) {
    return error.message;
  }
};

// get comment

export const createCommentApi = async (data: CommentCreate) => {
  try {
    const axiosServiceAuth = setAuthToken();
    const response = await axiosServiceAuth.post(
      `/articles/${data.slug}/comments`,
      data.comment
    );
    return response.data.comment;
  } catch (error) {
    return error.message;
  }
};

export const delteCommentApi = async (data: payloadCommentDelete) => {
  try {
    const axiosServiceAuth = setAuthToken();
    const response = await axiosServiceAuth.delete(
      `/articles/${data.payload.slug}/comments/${data.payload.commentId}`
    );
    return response.data;
  } catch (error) {
    return error.message;
  }
};

export const followArticleApi = async (data: PayloadArticleFollow) => {
  try {
    const axiosServiceAuth = setAuthToken();
    let response;
    if (data.followed) {
      response = await axiosServiceAuth.delete(`/profiles/${data.username}/follow`);
    } else {
      response = await axiosServiceAuth.post(`/profiles/${data.username}/follow`);
    }
    return response.data.profile;
  } catch (error) {
    return error.message;
  }
};

export const favoriteArticleApi = async (data: PayloadArticleFavorite) => {
  try {
    const axiosServiceAuth = setAuthToken();
    let response;
    if (data.checked) {
      response = await axiosServiceAuth.delete(
        `/articles/${data.slug}/favorite`
      );
    } else {
      response = await axiosServiceAuth.post(`/articles/${data.slug}/favorite`);
    }
    return response.data;
  } catch (error) {
    return error.message;
  }
};

// Get article by slug
export const GetArticleSlug = async (slug: string) => {
  try {
    const response = await axiosService.get(`/articles/${slug}`);
    return response.data;
  } catch (error) {
    return error.response;
  }
};

// Edit article by slug
export const EditArticleSlug = async (payload: {
  article: NewArticleInterface;
  slug: string;
}) => {
  try {
    const axiosServiceAuth = setAuthToken();
    const response = await axiosServiceAuth.put(`/articles/${payload.slug}`, {
      article: payload.article,
    });
    return response.data;
  } catch (error) {
    console.log(error);
  }
};

export const DeleteArticleSlug = async (slug: string) => {
  try {
    const axiosServiceAuth = setAuthToken();
    const response = await axiosServiceAuth.delete(`/articles/${slug}`);
    return response.data;
  } catch (error) {
    console.log(error);
  }
};

// get favorite by author
