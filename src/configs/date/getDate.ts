export const convertDate = (date: string) => {
  const newDate = new Date(date);
  return newDate.toDateString()
}
