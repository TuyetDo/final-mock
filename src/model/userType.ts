export interface userInputInfo {
  username?: string;
  email: string;
  password: string;
}

export interface userAuthResponse {
  email: string;
  image: string | null;
  token: string;
  username: string;
  bio: string | null;
}

export interface userAuthUpdate {
  email: string;
  image: string | null;
  username: string;
  bio: string | null;
  password: string;
}
