

export interface ArticlesResponse {
  articles: Article[];
  articlesCount: number;
}

export interface ArticleFavorite {
  article: Article;
}

export interface PayloadArticleFavorite {
  slug: string;
  checked: boolean;

}

export interface PayloadArticleFollow {
  username: string;
  followed: boolean;
}

export interface ArticleFollow {
  profile: Author;
}
export interface Article {
  title: string;
  slug: string;
  body: string;
  createdAt: string;
  updatedAt: string;
  tagList: string[];
  description: string;
  author: Author;
  favorited: boolean;
  favoritesCount: number;
}
export interface ArticleSlug {
  article: Article
}
export interface Author {
  username: string;
  bio?: string;
  image: string;
  following: boolean;
}

//get comments
export interface Comments {
  author: Author;
  id: number;
  updatedAt: string;
  createdAt: string;
  body: string;
}

export interface CommentsResponse {
  comments: Comments[];
}

// create comment
export interface CommentCreate {
  comment: {
    comment: {
      body: string;
    };
  };
  slug: string;
}

export interface CommentDelete {
  slug: string;
  commentId: number;
}

export interface payloadCommentDelete {
  type: string;
  payload: CommentDelete;
}
