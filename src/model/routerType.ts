import { ComponentType } from "react";



export interface IRouter {
    path: string;
    exact?: boolean;
    private?: boolean;
    redirect?: string;
    main: ({ history }: {
      history: any;
  }) => JSX.Element;
    component?: ComponentType<ComponentType<any>>;

}

