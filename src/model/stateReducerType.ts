export interface StateReducer {
  error: any;
  email: string | null;
  username: string | null;
  image: string | null;
  isLogin: boolean;
  bio: string | null;
  success?: any;
}
