export interface NewArticleInterface {
  title: string;
  description: string;
  body: string;
  tagList: string[];
}

export interface ActionNameNotify {
  action: string;
}
